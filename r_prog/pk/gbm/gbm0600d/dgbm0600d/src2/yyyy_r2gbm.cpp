
#include<string>
#include<vector>
#include<iostream>


#include "gbm.h"
#include "gbm_engine.h"
#include "gaussian.h"
 
/*
void getDataset(CDataset *pData, int cRows, int cCols ) {
    
  // allocate spaces
  double *adX      = new double[cRows*cCols];
  int    *aiXOrder = new int[cRows];
  double *adY      = new double[cRows];
  double *adOffset = new double[cRows];
  double *adWeight = new double[cRows];
  double *adMisc   = new double[3];
  
  int *acVarClasses = new int[cCols];
  int *alMonotoneVar = new int[cCols];

  // [] initialize values
  for(int iCtr=0; iCtr<(cRows*cCols); iCtr++) {
    adX[iCtr] = iCtr;
  }
  adY[0] = 0;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 1;

   
   //CDataset *pData = new CDataset();
   pData->SetData(adX,
                  aiXOrder,
                  adY,
                  adOffset,
                  adWeight,
                  adMisc,
                  cRows,
                  cCols,
                  acVarClasses,
                  alMonotoneVar);

   //double d11 = 3;
   //pData->Entry(1,1,d11);
   //std::cout<< "d11" << d11 << std::endl;
  
}


int main1() {

   std::cout << "Hello !\n";

   int cRows=4;
   int cCols=2;
   int cTrees=1; // number of trees to build

   // [] Dataset
   std::cout << " dataset " << std::endl;
   CDataset *pData = new CDataset();
   getDataset(pData, cRows, cCols);
 
   // [] Distribution
   std::cout << " distribution " << std::endl;
   CDistribution *pDist = new CGaussian(); 

   
   // []
   std::cout << " initialize " << std::endl;
   double dLambda = 0.1;
   double dNumTrain = 4;
   double dBagFraction =0.75;
   unsigned long cLeaveDepth = 3;
   unsigned long cMinsObsInNode = 3;  
   CGBM *pGBM = new CGBM();
   pGBM->Initialize(pData,
		    pDist,
		    dLambda,
		    dNumTrain,
		    dBagFraction,
		    cLeaveDepth,
		    cMinsObsInNode);

   // []
   int cNodes = 0;
   double *adF = new double[cRows]; // predictions
   for(int iT=0; iT<cTrees; iT++) {
     std::cout << " iT " << iT << std::cout ;
     double dTrainError = 0.0;
     double dValidError=0.0;
     double dOOBagImprove =0.0;

     pGBM->iterate(adF,
                 dTrainError,
                 dValidError,
                 dOOBagImprove,
                 cNodes);

   }


   std::cout << "Bye !\n";
   
   return 0;
}
*/


int main() {
  
  int cRows=4;
  int cCols=2;
  int cTrees=10; // number of trees to build
  int cTrain = 3;
  
  double *adY = new double[cRows];
  double *adOffset = NULL;
  double *adX = new double[cRows*cCols];
  int *aiXOrder = new int[cRows*cCols];
  double *adWeight = new double[cRows];
  double *adMisc = NULL;
  
  int *acVarClasses = new int[cCols];
  int *alMonotoneVar = new int[cCols];
  char pszFamily [] = "gaussian";
  int cLeavesDepth = 1;
  int cMinObsInNode = 1;
  double dShrinkage = 0.5;
  double dBagFraction = 0.5;
  
  CDataset *pData = new CDataset();
  CDistribution *pDist = NULL;


  // [] initialize values
  for(int iCtr=0; iCtr<(cRows*cCols); iCtr++) {


  }
  for(int cCtr=0; cCtr<cCols; cCtr++) {
    acVarClasses[cCols] = 0;
    alMonotoneVar[cCols] = 0;    
  }
  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;

  adX[0] = 0.1;
  adX[1] = 0.2;
  adX[2] = 0.3;
  adX[3] = 0.4;
  adX[4] = 0.4;
  adX[5] = 0.3;
  adX[6] = 0.2;
  adX[7] = 0.1;

  aiXOrder[0] = 0;
  aiXOrder[1] = 1;
  aiXOrder[2] = 2;
  aiXOrder[3] = 3;
  aiXOrder[4] = 3;
  aiXOrder[5] = 2;
  aiXOrder[6] = 1;
  aiXOrder[7] = 0;
  
  
  
  // initialize some things
   gbm_setup(adY,
                 adOffset,
		 adX,
		 aiXOrder,
		 adWeight,
		 adMisc,
		 cRows,
		 cCols,
		 acVarClasses,
		 alMonotoneVar,
		 pszFamily,
		 cTrees,
		 cLeavesDepth,
		 cMinObsInNode,
		 dShrinkage,
		 dBagFraction,
		 cTrain,
		 pData,
		 pDist);
   

   // []
   std::cout << " initialize " << std::endl;
   double dLambda = 0.1;
   double dNumTrain = cRows;
   //double dBagFraction =0.75;
   //unsigned long cLeaveDepth = 3;
   //unsigned long cMinsObsInNode = 3;  
   CGBM *pGBM = new CGBM();
   pGBM->Initialize(pData,
		    pDist,
		    dLambda,
		    dNumTrain,
		    dBagFraction,
		    cLeavesDepth,
		    cMinObsInNode);
   std::cout << " fInitialized " << pGBM->fInitialized <<  std::endl;

   //[]
   int cNodes = 0;
   double *adF = new double[cRows]; // predictions
   for(int iCtr=0; iCtr<cTrees; iCtr++) {
     std::cout << " iCtr " << iCtr << std::endl;
     double dTrainError = 0.0;
     double dValidError = 0.0;
     double dOOBagImprove = 0.0;

     pGBM->iterate(adF,
                 dTrainError,
                 dValidError,
                 dOOBagImprove,
                 cNodes);

     std::vector<double> vdF(adF,adF+sizeof adF / sizeof adF[0]);
     std::string         sdF(vdF.begin(), vdF.end());
     std::cout << "sdF"<< sdF << std::endl;
     printf("cNodes %d r%f s%f b%f\n",cNodes, dTrainError, dValidError, dOOBagImprove );
     printf("==============================================\n");

   }


   std::cout << "Bye !\n";
   return 0;     
}
