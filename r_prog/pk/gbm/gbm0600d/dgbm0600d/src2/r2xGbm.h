/*
 * r2xGbm.hpp
 *
 *  Created on: Apr 14, 2017
 *      Author: dsm
 */

#include<string>
#include<vector>
#include<iostream>
#include<iterator>
#include<numeric>
#include<algorithm>
#include<sstream>
#include<string>




// log4cpp
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

//using namespace config4cpp;
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

#include "gbm.h"
#include "gbm_engine.h"
#include "gaussian.h"
#include "f711_model_tree.h"
#include "f712_model_forest.h"

#include <fstream>


#ifndef SRC2_R2XGBM_HPP_
#define SRC2_R2XGBM_HPP_


/**
 *
 * print an array
 * sorting array and return its index
 * randomly generate a dataset
 * generate dataset based on id and row information
 *
 */
class r2xGbm {
	public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("r2xGbm"));

	// logging an array value
	void db_log_array_double(string sArr, double * pArr, int iLen);
	void db_log_array_int(string sArr, int* pArr, int iLen);
	template <typename T> void db_print_array( T * pa, int iLen );

	void db_print_array_log(double *pa, int iLen);
	void db_print_order_log(int *pi, int iLen);
	void db_print_array_sorted_log(double *pa, int *pi, int iRow, int iCol);

	template <typename T> std::vector<size_t> sort_indexes(const std::vector<T> &v);
	void db_sort_array( double * adX, int iLen,  int* iOutIdx );
	void db_sort_mat_o_idx(double *dX, int iRow, int iCol, int* piOutIdx);

	//
	void db_init46(double *adX, double *adY, double *adWeight);
	void db_init64(double *adX, double *adY, double *adWeight);
	void db_init104(double *adX, double *adY, int *adZ, double *adWeight);
	void db_init204(double *adX, double *adY, double *adWeight);
	void db_save(int iSizeObs, int iSizeFeat, double *adX, double *adY, int *adZ, std::string sName);

	int db_num_lines(std::string sName);
	int db_num_features(std::string sName);
	const char* db_toString(int iObsLen, int iFeatLen, double *adX, double *adY, double *adW, double *aiQ);
	void db_fromFile(std::string sName, int iObsLen, int iFeatLen, double *adY, double *aiQ, double *adW, double *adX );
	void db_fromFile_try();

	//void getDataset(CDataset *pData, int cRows, int cCols );

	void utl_init_array_to_zeros(int *dArray, int iSize);
	void utl_init_array_to_zeros(double *dArray, int iSize);
};



/**
 * log an array
 */
void r2xGbm::db_log_array_double(string sArr, double * pArr, int iLen) {

	sArr += " ";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pArr[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

void r2xGbm::db_log_array_int(string sArr, int * pArr, int iLen) {

	sArr += " ";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pArr[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

/*
 * print an array
 */
template <typename T>
void r2xGbm::db_print_array( T * pa, int iLen) {
	mLog.debug("db_print_array");
  std::vector<T> va(pa,pa+iLen);
  std::copy(va.begin(), va.end(), std::ostream_iterator<T>(std::cout, " "));
  std::cout << std::endl;
}


void r2xGbm::db_print_array_log(double * pa, int iLen) {

	string sArr = "";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pa[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}


void r2xGbm::db_print_array_log(double * pa, int iLen) {

	string sArr = "";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pa[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

void r2xGbm::db_print_order_log(int * pi, int iLen) {

	string sArr = "";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pi[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

/*
 * pa : array
 * pi : index
 * iLen : length of array
 */
void r2xGbm::db_print_array_sorted_log(double *pa, int *pi, int iRow, int iCol) {

	string sArr = "";
	for(int i=0; i<iCol; i++) {
		int   * iArr = new int[iRow];
		double* dArr = new double[iRow];
		for(int j=0; j<iRow; j++) {
			int iTmp = i*iRow+j;
			dArr[j] = pa[iTmp];
			iArr[j] = pi[iTmp];
		}
		for(int j=0; j<iRow; j++) {
			sArr += std::to_string(dArr[iArr[j]])+" ";
		}
	}
	mLog.info("%s",sArr.c_str());
}


/**
 * sorting an array
 */
template <typename T>
std::vector<size_t> r2xGbm::sort_indexes(const std::vector<T> &v) {

  // initialize original index locations
  std::vector<size_t> idx(v.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}


/**
 */
void r2xGbm::db_sort_array( double * adX, int iLen,  int* iOutIdx ) {
  std::vector<double> vdF(adX,adX+iLen);
  std::vector<size_t> idx = sort_indexes(vdF);
  // for (auto i: idx) { std::cout << i << std::endl; }
  // size_t *iOutIdx = &idF[0];

  for(int i=0; i<idx.size(); i++) {
    iOutIdx[i] = static_cast<int>( idx[i]);
  }
}

/**
 * dX       :
 * iRow     : number of row
 * iCol     : number of column
 * piOutIdx : output index
 */
void r2xGbm::db_sort_mat_o_idx(double *dX, int iRow, int iCol, int* piOutIdx) {

	//printf("iRow:%d iCol:%d \n",iRow,iCol);
	for(int i=0; i<iCol; i++) {
		// [] get an array of values
		double* dTmp = new double[iRow];
		for(int j=0; j<iRow; j++) {
			dTmp[j] = dX[i*iRow+j];
		}

		// [] do sorting
		int* piTmpIdx = new int[iRow];
		db_sort_array(dTmp, iRow,  piTmpIdx );
		// [] copy to output
		for(int j=0; j<iRow; j++) {
			piOutIdx[i*iRow+j]=piTmpIdx[j];
		}
	}

}



void r2xGbm::db_init46(double * adX, double * adY, double * adWeight) {

  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;

   adX[0] = 0.11;
   adX[1] = 0.13;
   adX[2] = 0.12;
   adX[3] = 0.14;

   adX[4] = 0.28;
   adX[5] = 0.26;
   adX[6] = 0.27;
   adX[7] = 0.29;

   adX[8] = 0.31;
   adX[9] = 0.33;
   adX[10] = 0.34;
   adX[11] = 0.32;

   adX[12] = 0.46;
   adX[13] = 0.48;
   adX[14] = 0.49;
   adX[15] = 0.47;

   adX[16] = 0.51;
   adX[17] = 0.53;
   adX[18] = 0.54;
   adX[19] = 0.52;

   adX[20] = 0.66;
   adX[21] = 0.68;
   adX[22] = 0.69;
   adX[23] = 0.67;

}


void r2xGbm::db_init64(double * adX, double * adY, double * adWeight) {

  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;
  adY[4] = 1;
  adY[5] = 0;

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;
  adWeight[4] = 1;
  adWeight[5] = 1;

   adX[0] = 0.11;
   adX[1] = 0.13;
   adX[2] = 0.12;
   adX[3] = 0.14;
   adX[4] = 0.18;
   adX[5] = 0.16;

   adX[6] = 0.27;
   adX[7] = 0.29;
   adX[8] = 0.21;
   adX[9] = 0.23;
   adX[10] = 0.24;
   adX[11] = 0.22;

   adX[12] = 0.36;
   adX[13] = 0.38;
   adX[14] = 0.39;
   adX[15] = 0.37;
   adX[16] = 0.51;
   adX[17] = 0.53;

   adX[18] = 0.44;
   adX[19] = 0.42;
   adX[20] = 0.46;
   adX[21] = 0.48;
   adX[22] = 0.49;
   adX[23] = 0.47;

}


void r2xGbm::db_init104(double *adX, double *adY, int *adZ, double * adWeight) {

	int iRow = 10;
	int iCol = 4;

	int i=0;
	int j=0;
	for(i=0; i<iRow; i++) {
		adWeight[i] = 1;
		adY[i] = 0;
		if( i % 2 == 0) {
			adY[i] = 1;
		}
		for(j=0; j<iCol; j++) {
			adX[i*iCol+j] = (rand () % 100)*1.0/100;
		}
	}

	for(i=0; i<5; i++) {
		adZ[i] = 1;
	}
	for(i=5; i<10; i++) {
		adZ[i] = 2;
	}

	// [] printing out
	for(i=0; i<iRow; i++) { // iterate observations
		//mLog.info(" i %d y %f w %f", i, adY[i], adWeight[i]);
		std::string sRow="";
		for(j=0; j<iCol; j++) { // iterate variables
			//sRow = sRow + std::to_string(adX[i*iCol+j]) + " ";
			//sRow = sRow + std::to_string(i*iCol+j) + " ";
			//sRow = sRow + std::to_string(j*iRow+i) + " ";
			sRow = sRow + std::to_string(adX[j*iRow+i]) + " ";
		}
		mLog.info(" i:%d y:%f w:%f x:%s", i, adY[i], adWeight[i], sRow.c_str());
	}

}

/**
 * Matrix of 20 x 4
 */
void r2xGbm::db_init204(double * adX, double * adY, double * adWeight) {

	int i,j;
	for(i=0; i<10; i++) {
		adY[i] = 1;
		adWeight[i] = 1;
	}

	for(i=10; i<20; i++) {
		adY[i] = 0;
		adWeight[i] = 1;
		//mLog.info(" i %d y %f w %f", i, adY[i], adWeight[i]);
	}

	for(i=0; i<80; i++) {
		adX[i] = (rand () % 100)*1.0/100;
		//mLog.info(" i %d x %f ", i, adX[i]);
	}

	for(i=0; i<20; i++) {
		mLog.info(" i %d y %f w %f", i, adY[i], adWeight[i]);
		for(j=0; j<4; j++) {
			mLog.info(" %f", adX[i*4+j]);
		}
	}

}

/*

void r2xGbm::getDataset(CDataset *pData, int cRows, int cCols ) {

  // allocate spaces
  double *adX      = new double[cRows*cCols];
  int    *aiXOrder = new int[cRows];
  double *adY      = new double[cRows];
  double *adOffset = new double[cRows];
  double *adWeight = new double[cRows];
  double *adMisc   = new double[3];

  int *acVarClasses = new int[cCols];
  int *alMonotoneVar = new int[cCols];

  // [] initialize values
  for(int iCtr=0; iCtr<(cRows*cCols); iCtr++) {
    adX[iCtr] = iCtr;
  }
  adY[0] = 0;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 1;


   //CDataset *pData = new CDataset();
   pData->SetData(adX,
                  aiXOrder,
                  adY,
                  adOffset,
                  adWeight,
                  adMisc,
                  cRows,
                  cCols,
                  acVarClasses,
                  alMonotoneVar);

   //double d11 = 3;
   //pData->Entry(1,1,d11);
   //std::cout<< "d11" << d11 << std::endl;

}
*/


void r2xGbm::db_save(int iSizeObs, int iSizeFeat, double *adX, double *adY, int *adZ, std::string sName) {

	// [] print out
	std::string sOut="";
	for(int iCtrObs=0; iCtrObs<iSizeObs; iCtrObs++) {
		sOut = sOut + std::to_string((int) adY[iCtrObs])+" qid:";
		sOut = sOut + std::to_string( adZ[iCtrObs])+" ";
		for(int iCtrFeat=0; iCtrFeat<iSizeFeat; iCtrFeat++) {
			sOut = sOut + std::to_string(iCtrFeat+1) + ":" + std::to_string(adX[iCtrFeat*iSizeObs+iCtrObs])+" ";
		}
		sOut = sOut + "#docid = "+ std::to_string(iCtrObs) + "\n";
	}
	mLog.info("%s",sOut.c_str());

	std::ofstream myfile;
	myfile.open (sName);
	myfile << sOut;
	myfile.close();

}

void r2xGbm::utl_init_array_to_zeros(int *dArray, int iSize) {
	for(int cCtr=0; cCtr<iSize; cCtr++) {
		dArray[cCtr] = 0;
	}
}

void r2xGbm::utl_init_array_to_zeros(double *dArray, int iSize) {
	for(int cCtr=0; cCtr<iSize; cCtr++) {
		dArray[cCtr] = 0.0;
	}
}


void r2xGbm::db_fromFile_try() {

	// []
	std::string sName="../dat/letor1/r1a.txt";
	// [] number of observation
	int iObsLen = db_num_lines(sName);
	int iFeatLen = db_num_features(sName);
	//cout << iObsLen << " x "<< iFeatLen << endl;

	// []
	double *aY = new double[iObsLen];
	double *aQ = new double[iObsLen];
	double *aW = new double[iObsLen];
	double *aX = new double[iObsLen*iFeatLen];
	//std::vector<int> dY;
	//std::vector<int> dQ;
	//std::vector<std::vector<double>> dX (iObsLen, std::vector<double>(iFeatLen) );

	db_fromFile(sName, iObsLen, iFeatLen, aY, aQ, aW, aX);
	cout << db_toString(iObsLen, iFeatLen, aX, aY, aW, aQ) << endl;
}

/**
 * read data from a file name
 * create
 *  aY : relevance levels
 *  aW : weight
 *  aQ : query id
 *  aX : feature matrix
 */
void r2xGbm::db_fromFile(std::string sName, int iObsLen, int iFeatLen,
		double *adY,  double *adQ, double *adW, double *adX) {

	// []
	std::ifstream infile(sName);
	std::string aLine; // aLine from an input file
	std::string ssChunk; // temporary string
	std::string ssKey; // key
	std::string ssVal; // value

	for ( int iCtrObs=0; std::getline(infile, aLine); iCtrObs++ ) {

		// [] output the line
		//std::cout << aLine << std::endl;

		// [] weight
		adW[iCtrObs] = 1.0;

		// [] Relevance levels
		stringstream ssLine(aLine);
		getline(ssLine, ssChunk, ' ');
		adY[iCtrObs] = std::stod(ssChunk);
		//dY.push_back(std::stoi(ssChunk));
		//cout << std::stoi(ssChunk) <<endl;

		// QID
		getline(ssLine, ssChunk, ' ');
		stringstream ssQid(ssChunk);
		getline(ssQid, ssKey, ':');
		getline(ssQid, ssVal, ':');
		adQ[iCtrObs] = std::stod(ssVal);
		//dQ.push_back(std::stoi(ssVal));
		//cout << std::stoi(ssVal) <<endl;

		//std::vector<double> dXInst;
		for( int iCtrFeat=0; iCtrFeat<iFeatLen; iCtrFeat++ ) {
			getline(ssLine, ssChunk, ' ');
			stringstream ssQid(ssChunk);
			getline(ssQid, ssKey, ':');
			getline(ssQid, ssVal, ':');
			//cout << ssVal << endl;
			//cout << iCtrFeat*iObsLen+iCtrObs << endl;
			adX[iCtrFeat*iObsLen+iCtrObs] =std::stod(ssVal);
			//dXInst.push_back(std::stod(ssVal));
		}

		//dX.push_back(dXInst);
	}

	// []
	//std::copy(dQ.begin(), dQ.end(), std::ostream_iterator<int>(std::cout, " "));
	//std::copy(dY.begin(), dY.end(), std::ostream_iterator<int>(std::cout, " "));

}

/**
 * count the number of lines in a data file
 */
int r2xGbm::db_num_lines(std::string sName) {

	std::ifstream infile(sName);

	int iCtr=0;
	std::string aLine;
	while (std::getline(infile, aLine)) {
		iCtr++;
	}

	return iCtr;
}


/**
 * return number of observation
 */
int r2xGbm::db_num_features(std::string sName) {

	int iCtr=0;
	std::string aLine; // aLine from an input file
	std::string ssChunk; // temporary string

	// [] open the file
	std::ifstream infile(sName);

	// [] get the first line
	std::getline(infile, aLine);

	stringstream ssLine(aLine);
	while( getline(ssLine, ssChunk, ' ') ) {
		if( ssChunk.find("#") == 0 ) {
			break;
		} else {
			iCtr++;
		}
	}

	return iCtr-2;
}

/*
 * print out data information
 */
const char* r2xGbm::db_toString(int iObsLen, int iFeatLen, double *adX, double *adY, double *adW, double *adQ) {

	std::string sOut = "";

	// []
	for(int iCtrObs=0; iCtrObs<iObsLen; iCtrObs++) {
		sOut = sOut + std::to_string(adY[iCtrObs])+ " ";
		sOut = sOut + std::to_string(adQ[iCtrObs])+ " ";
		sOut = sOut + std::to_string(adW[iCtrObs])+ " ";
		for( int iCtrFeat=0; iCtrFeat<iFeatLen; iCtrFeat++) {
			sOut = sOut + std::to_string( adX[iCtrFeat*iObsLen+iCtrObs]  ) + " ";
		}
		sOut = sOut + "\n";
	}

	return sOut.c_str();
}

#endif /* SRC2_R2XGBM_HPP_ */
