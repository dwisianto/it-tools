
#include <iostream>

using namespace std;

class c1a {
public:
	void t1a();
	void t1b();
};


void c1a::t1a() {
	cout<< "HW" << endl;
}

void c1a::t1b() {
	cout<< "HW" << endl;
}

int main() {

   cout << "Hello \n";
   c1a* c1 = new c1a();
   c1->t1a();
   cout << "Bye\n";

   return 0;
}
