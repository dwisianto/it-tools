
#include<iostream>
#include "dataset.h"
 
int main() {

   std::cout << "Hello Dataset!\n";

   int cRows=4;
   int cCols=2;
   
   double *adX      = new double[cRows];
   int    *aiXOrder = new int[cRows];
   double *adY      = new double[cRows];
   double *adOffset = new double[cRows];
   double *adWeight = new double[cRows];
   double *adMisc   = new double[3];

   int *acVarClasses = new int[cCols];
   int *alMonotoneVar = new int[cCols];
   
   CDataset *pData = new CDataset();
   pData->SetData(adX,
                  aiXOrder,
                  adY,
                  adOffset,
                  adWeight,
                  adMisc,
                  cRows,
                  cCols,
                  acVarClasses,
                  alMonotoneVar);

   double d11 = 3;
   pData->Entry(1,1,d11);
   std::cout<< "d11" << d11 << std::endl;
   

   std::cout << "Bye Dataset!\n";   
   return 0;
}
