
#include "xxx_r2cGbm.h"

#include "xxx_r2bGbm.h"


int main() {

   std::cout << "Hello !\n";

   int cRows=4;
   int cCols=2;

   double *adX = new double[cRows*cCols];
   adX[0] = 0.1;
   adX[1] = 0.3;
   adX[2] = 0.2;
   adX[3] = 0.4;
   adX[4] = 0.3;
   adX[5] = 0.1;
   adX[6] = 0.2;
   adX[7] = 0.4;
   //db_print(adX, cRows, cCols);
   db_print_array(adX,cRows*cCols);

   // sort and get index
   int *aiXOrder = new int[cRows*cCols];
   db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
   db_print_array(adX,cRows*cCols);
   
   std::cout << "Bye !\n";
   return 0;     
}
