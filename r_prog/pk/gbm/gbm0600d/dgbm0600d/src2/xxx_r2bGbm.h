
#include<string>
#include<vector>
#include<iostream>
#include<iterator>
#include<numeric>
#include<algorithm>

/*
 * print an array
 */
template <typename T>
void db_print_array( T * pa, int iLen) {
  std::vector<T> va(pa,pa+iLen);
  std::copy(va.begin(), va.end(), std::ostream_iterator<T>(std::cout, " "));
  std::cout << std::endl;   
}


/**
 * sorting an array
 */
template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v) {

  // initialize original index locations
  std::vector<size_t> idx(v.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}


/**
 */
void db_sort_array( double * adX, int iLen,  int* iOutIdx ) {
  std::vector<double> vdF(adX,adX+iLen);
  std::vector<size_t> idx = sort_indexes(vdF);
  // for (auto i: idx) { std::cout << i << std::endl; }  
  // size_t *iOutIdx = &idF[0];
  
  for(int i=0; i<idx.size(); i++) {
    iOutIdx[i] = static_cast<int>( idx[i]);
  }  
}
  




