


#include "r2xGbm.h"

using namespace std;
namespace pt = boost::property_tree;


/**
 * http://www.boost.org/doc/libs/1_64_0/doc/html/property_tree/tutorial.html
 * std::set<std::string> m_modules;
 */
struct gbf_settings {

    int m_crows;
    int m_ccols;

    std::string m_dbloc;
    std::string m_dbname;

    std::string m_log_loc;
    std::string m_result_loc;
    std::string m_result_tree;
    std::string m_result_forest;
    std::string m_result_eval;
    std::string m_result_pred;

    std::string m_resultTreePred;
	std::string m_resultTreeEval;
	std::string m_resultTreeDot;

	std::string m_resultForestPred;
	std::string m_resultForestEval;

    void load(const std::string &filename);
    void save(const std::string &filename);
    std::string toString();
};


// Use get_child to find the node containing the modules, and iterate over
// its children. If the path cannot be resolved, get_child throws.
// A C++11 for-range loop would also work.
//BOOST_FOREACH(pt::ptree::value_type &v, tree.get_child("debug.modules")) {
    // The data function is used to access the data stored in a node.
    //m_modules.insert(v.second.data());
//}
void gbf_settings::load(const std::string &filename) {

    // Create empty property tree object
    pt::ptree tree;

    // Parse the XML into the property tree.
    pt::read_xml(filename, tree);

    //
    m_crows = tree.get("gbf.lenrow", 0);
    m_ccols = tree.get("gbf.lencol", 0);

    //
    m_dbname = tree.get<std::string>("gbf.dbname");
    m_dbloc = tree.get<std::string>("gbf.dbloc");

    //
    m_log_loc       = tree.get<std::string>("gbf.logLoc");
    m_result_loc    = tree.get<std::string>("gbf.resultLoc");
    m_result_tree   = tree.get<std::string>("gbf.resultTree");
    m_result_forest = tree.get<std::string>("gbf.resultForest");
    m_result_eval   = tree.get<std::string>("gbf.resultEval");
    m_result_pred   = tree.get<std::string>("gbf.resultPred");

    m_resultTreePred   = tree.get<std::string>("gbf.resultTreePred");
	m_resultTreeEval   = tree.get<std::string>("gbf.resultTreeEval");
	m_resultTreeDot    = tree.get<std::string>("gbf.resultTreeDot");

	m_resultForestPred = tree.get<std::string>("gbf.resultForestPred");
	m_resultForestEval = tree.get<std::string>("gbf.resultForestEval");

}



/**
 *
 */
void gbf_settings::save(const std::string &filename) {

    // Create an empty property tree object.
    pt::ptree tree;

    // Put the simple values into the tree.
    // The integer is automatically converted to a string.
    // Note that the "debug" node is automatically created if it doesn't exist.
    //tree.put("debug.filename", m_crows);
    //tree.put("debug.level", m_ccols);
	tree.put("gbf.lenrow",m_crows);
	tree.put("gbf.lencol",m_ccols);

	tree.put("gbf.dbname",m_dbname);
	tree.put("gbf.dbloc",m_dbloc);

	tree.put("gbf.logLoc",m_log_loc);
	tree.put("gbf.resultLoc",m_result_loc);
	tree.put("gbf.resultTree",m_result_tree);
	tree.put("gbf.resultForest",m_result_forest );
	tree.put("gbf.resultEval",m_result_eval);
	tree.put("gbf.resultPred",m_result_pred);

	tree.put("gbf.resultTreePred",m_resultTreePred );
	tree.put("gbf.resultTreeEval",m_resultTreeEval );
	tree.put("gbf.resultTreeDot",m_resultTreeDot );
	tree.put("gbf.resultForestPred",m_resultForestPred );
	tree.put("gbf.resultForestEval",m_resultForestEval);

    // Add all the modules.
    // Unlike put, which overwrites existing nodes,
    // add adds a new node at the lowest level,
    // so the "modules" node will have multiple "module" children.
    //BOOST_FOREACH(const std::string &name, m_modules)
    //tree.add("debug.modules.module", name);

    // Write property tree to XML file
    pt::write_xml(filename, tree);
}

std::string gbf_settings::toString() {

	std::string sOut = "";

	sOut += " m_dbloc  "+m_dbloc+"\n";
	sOut += " m_dbname "+m_dbname+"\n";
	sOut += " m_crows  "+std::to_string(m_crows)+"\n";
	sOut += " m_ccols  "+std::to_string(m_ccols)+"\n";

	return sOut;
}


class r2xTst {

	public:

	// []
	//std::string initFileName = "log4cpp.properties";
	//log4cpp::PropertyConfigurator::configure(initFileName);
	//log4cpp::PropertyConfigurator::configure("log4cpp.properties");
	log4cpp::Category& mLogRoot = log4cpp::Category::getRoot();
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("r2xTst"));

	r2xGbm* mGbmUtl = new r2xGbm();


	void t1a_print_vector();
	void t1b_sort_index_array();
	void t1c_sort_index_matrix();
	void t1d_init_data46();
	void t1e_init_data46();
	void t1f_db_read_try();

	void t2a_gbm_run();
	void t2b_gbm_run();

	//void t2c_gbf_run();
	void t2c_gbf(gbf_settings );


	void t3a_transfer();

	void t4a_print();

	void t5a_plot();

};


// [] Configuration
//setlocale(LC_ALL, "");
//config4cpp::Configuration * cfg = config4cpp::Configuration::create();
int main() {

	// [] Logging
	std::string initFileName = "conf/c3.properties";
	log4cpp::PropertyConfigurator::configure(initFileName);

	// [] Configuration
	gbf_settings cfg;
	std::string sCfgName="conf/c3.xml";
	cfg.load(sCfgName);
	cfg.save(sCfgName+".tmp");

	// []
	r2xTst * tt = new r2xTst();
	std::string sCase="t2c";
	tt->mLog.info("Hello Case %s", sCase.c_str());

	if ( sCase == "t1a")      { tt->t1a_print_vector(); }
	else if ( sCase == "t1b") { tt->t1b_sort_index_array(); }
	else if ( sCase == "t1c") { tt->t1c_sort_index_matrix(); }
	else if ( sCase == "t1d") { tt->t1d_init_data46(); }
	else if ( sCase == "t1e") { tt->t1e_init_data46(); }
	else if ( sCase == "t1f") { tt->t1f_db_read_try(); }
	else if ( sCase == "t2a") { tt->t2a_gbm_run(); }
	else if ( sCase == "t2b") { tt->t2b_gbm_run(); }
	else if ( sCase == "t2c") { tt->t2c_gbf(cfg); }

	// []
	tt->mLog.info("Bye");
	log4cpp::Category::shutdown();

	// []
	return 0;
}




int main0Settings() {

	cout << "Hello World" << endl;

	// [] Configuration
	gbf_settings cfg;
	std::string sCfgName="conf/c2.xml";
	cfg.load(sCfgName);
	cfg.save(sCfgName+".tmp");

	// []
	return 0;
}

int main0Logging() {

}


int main0() {

	cout << "Hello World" << endl;

	// []
	return 0;
}




/**
 * Printing out a vector of double
 */
void r2xTst::t1a_print_vector() {
	mLog.debug("t1a_print_vector");

	int cRows=4;
	int cCols=1;
	int cTrees=1; // number of trees to build

	double *adX = new double[cRows*cCols];
	adX[0] = 0.1;
	adX[1] = 0.2;
	adX[2] = 0.3;
	adX[3] = 0.4;

	vector<double> vdF(adX,adX+3);
	//std::vector<double> vdF(adX,adX+(sizeof(adX)/ sizeof(adX[0])));
	//std::string         sdF(vdF.begin(), vdF.end());
	//std::cout << "sdF"<< sdF << std::endl;
	copy(vdF.begin(), vdF.end(), ostream_iterator<double>(cout, " "));

}

/**
 * using the class to print out array
 */
void r2xTst::t1b_sort_index_array() {
	mLog.info("t1b_sort_index_array");
	r2xGbm* gg = new r2xGbm();

	int cRows=4;
	int cCols=1;

	double *adX = new double[cRows*cCols];
	adX[0] = 0.1;
	adX[1] = 0.3;
	adX[2] = 0.2;
	adX[3] = 0.4;
	gg->db_print_array(adX, cRows*cCols);

	int* iIdx  = new int[cRows];
	gg->db_sort_array( adX, cRows*cCols, iIdx );
	gg->db_print_array( iIdx, cRows*cCols);
	// output to a log file
	gg->db_print_array_log(adX, cRows*cCols);
	gg->db_print_array_sorted_log(adX, iIdx, cRows,cCols);

}

/**
 * verify that ordering is correct
 */
void r2xTst::t1c_sort_index_matrix() {

	r2xGbm* gg = new r2xGbm();

	int cRows=5;
	int cCols=2;

	//X1 <- c(0.6,0.4,0.2,0.8,1.0)
	//X2 <- c(0.3,0.9,0.5,0.1,0.7)
	//Y <- c(1,2,3,4,5)
	//data <- data.frame(Y=Y,X1=X1,X2=X2)
	double adX[10]   = {0.6,0.4,0.2,0.8,1.0,0.3,0.9,0.5,0.1,0.7};
	double aiXgt[10] = {2,1,0,3,4,3,0,2,4,1};

	// sort and get index
	int *aiXOrder = new int[cRows*cCols];
	gg->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	gg->db_print_array(adX,cRows*cCols);
	gg->db_print_array(aiXOrder,cRows*cCols);
	gg->db_print_array_log(adX, cRows*cCols);
	gg->db_print_array_sorted_log(adX, aiXOrder, cRows,cCols);

}


void r2xTst::t1d_init_data46() {

	   int cRows=4;
	   int cCols=6;

	   double *adX = new double[cRows*cCols];
	   double *adY = new double[cRows];
	   double *adW = new double[cRows];
	   mGbmUtl->db_init46( adX, adY, adW);
	   mGbmUtl->db_print_array(adX,cRows*cCols);

	   // sort and get index
	   int *aiXOrder = new int[cRows*cCols];
	   mGbmUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	   mGbmUtl->db_print_array_log(adX, cRows*cCols);
	   mGbmUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);

}


void r2xTst::t1e_init_data46() {

	  // [] Data :: Constants
	  int cRows  = 4;
	  int cCols  = 6;
	  int cTrain = 3;
	  double dBagFraction = 0.5;

	  // [] Trees ::
	  int cTrees          = 1; // number of trees to build
	  int cLeavesDepth    = 1;
	  int cMinObsInNode   = 1;
	  double dShrinkage   = 0.5;

	  // [] Data :: Structures
	  char pszFamily []   = "gaussian";
	  CDistribution *pDist = NULL;

	  CDataset *pData      = new CDataset();

	  double *adY      = new double[cRows];
	  double *adWeight = new double[cRows];

	  double *adX      = new double[cRows*cCols];
	  int *aiXOrder    = new int[cRows*cCols];

	  double *adOffset = NULL;
	  double *adMisc   = NULL;

	  int *acVarClasses   = new int[cCols];
	  int *alMonotoneVar  = new int[cCols];

	  // [] initialize values
	  for(int cCtr=0; cCtr<cCols; cCtr++) {
	    acVarClasses[cCtr] = 0;
	    alMonotoneVar[cCtr] = 0;
	  }
	  mGbmUtl->db_init46(adX, adY, adWeight);
	  mGbmUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	  //mGbmUtl->db_print_array(aiXOrder,cRows*cCols);
	  mGbmUtl->db_print_array_log(adX, cRows*cCols);
	  mGbmUtl->db_print_order_log(aiXOrder, cRows*cCols);
	  mGbmUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);

}

void r2xTst::t1f_db_read_try() {

	//mGbmUtl->db_read(sFile);
		// []
		std::string sFile="../dat/letor1/r1a.txt";
		//r2xTst * tt = new r2xTst();
		//tt->mGbmUtl->db_read(sFile);
		mGbmUtl->db_fromFile_try();

}



void r2xTst::t2a_gbm_run() {

	  // [] Data :: Constants
	  int cRows           = 10; // 4,6,10, 20
	  int cCols           = 4; // 6,4,4, 4
	  int cTrain          = cRows;
	  double dBagFraction = 1.0;

	  // [] Trees ::
	  int cTrees          = 10; // number of trees to build
	  int cLeavesDepth    = 5;
	  int cMinObsInNode   = 1;
	  double dShrinkage   = 0.5;
	  double dLambda      = 0.1; // Is dLambda the same as dShrinkage

	  std::string sDbName="logs/dbRaw";

	  /*
	  try {
		  cfg->parse("conf/c1.cfg");
		  const char *    scope    = "dgbm";
		  dBagFraction    = (double) cfg->lookupFloat(scope, "dBagFraction");

		  mLog.info("Config4cpp");
		  mLog.info("dBagFraction : %f ", dBagFraction );

	  } catch(const config4cpp::ConfigurationException & ex) {
		  std::cout << ex.c_str() << endl;
	  }
	  */



	  // [] Data :: Structures
	  char pszFamily []    = "gaussian";
	  CDistribution *pDist = NULL;

	  CDataset *pData      = new CDataset();

	  double *adY          = new double[cRows];
	  double *adWeight     = new double[cRows];

	  double *adX          = new double[cRows*cCols];
	  int *aiXOrder        = new int[cRows*cCols];

	  //int *adZ             = new int[cRows];
	  int *adQryId         = new int[cRows];

	  double *adOffset = NULL;
	  double *adMisc   = NULL;

	  int *acVarClasses   = new int[cCols];
	  int *alMonotoneVar  = new int[cCols];

	  // [] initialize values
	  // for(int cCtr=0; cCtr<cCols; cCtr++) { acVarClasses[cCtr] = 0; alMonotoneVar[cCtr] = 0; }
	  mGbmUtl->utl_init_array_to_zeros(acVarClasses, cCols);
	  mGbmUtl->utl_init_array_to_zeros(alMonotoneVar, cCols);

	  if(cRows == 4 ) {
		  mLog.info("db_init::cRows == 4");
		  mGbmUtl->db_init46(adX, adY, adWeight);
	  } else if(cRows == 6 ) {
		  mLog.info("db_init::cRows == 6");
		  mGbmUtl->db_init64(adX, adY, adWeight);
	  } else if( cRows == 10 ) {
		  mLog.info("db_init::cRows == 10");
		  mGbmUtl->db_init104(adX, adY, adQryId, adWeight);
	  } else if( cRows == 20 ) {
		  mLog.info("db_init::cRows == 20");
		  mGbmUtl->db_init204(adX, adY, adWeight);
	  }
	  mGbmUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	  mGbmUtl->db_print_array_log(adX, cRows*cCols);
	  mGbmUtl->db_print_order_log(aiXOrder, cRows*cCols);
	  mGbmUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);
	  mGbmUtl->db_save(cRows, cCols, adX, adY, adQryId, sDbName);

	  // initialize some things
	   gbm_setup(adY,
	             adOffset,
			 adX,
			 aiXOrder,
			 adWeight,
			 adMisc,
			 cRows,
			 cCols,
			 acVarClasses,
			 alMonotoneVar,
			 pszFamily,
			 cTrees,
			 cLeavesDepth,
			 cMinObsInNode,
			 dShrinkage,
			 dBagFraction,
			 cTrain,
			 pData,
			 pDist);


	   // []
	   mLog.info(" initialize ");

	   double dNumTrain = cTrain;
	   //double dBagFraction =0.75;
	   //unsigned long cLeaveDepth = 3;
	   //unsigned long cMinsObsInNode = 3;
	   CGBM *pGBM = new CGBM();
	   pGBM->Initialize(pData,
			    pDist,
			    dLambda,
			    dNumTrain,
			    dBagFraction,
			    cLeavesDepth,
			    cMinObsInNode);
	   mLog.info("pGBM->fInitialized ");// pGBM->fInitialized

	   //[]
	   int cNodes = 0;
	   double *adF = new double[cRows]; // predictions
	   mGbmUtl->utl_init_array_to_zeros(adF, cRows);
	   //for(int iCtr=0; iCtr<cRows; iCtr++) {		   adF[iCtr] = 0.0;	   }

	   CMdlForest *dMdlFrst = new CMdlForest();
	   dMdlFrst->initialize(cTrees);
	   for(int iCtr=0; iCtr<cTrees; iCtr++) {
		   mLog.info("iCtr %d", iCtr);

	     double dTrainError = 0.0;
	     double dValidError = 0.0;
	     double dOOBagImprove = 0.0;

	     pGBM->iterate(adF,
	                 dTrainError,
	                 dValidError,
	                 dOOBagImprove,
	                 cNodes);

	     std::vector<double> vdF(adF,adF+sizeof adF / sizeof adF[0]);
	     std::string         sdF(vdF.begin(), vdF.end());
	     //std::cout << "sdF"<< sdF << std::endl;
	     mLog.info("cNodes %d r %f s %f b %f",cNodes, dTrainError, dValidError, dOOBagImprove );
	     mLog.info("adF %s", sdF.c_str());
	     pGBM->PrintTree();
	     mLog.info("==============================================");

		 // []
	     std::string sFileDot="logs/dotMdlTree"+std::to_string(iCtr);
	     mLog.info(" dotMdlTree %s ", sFileDot.c_str());
	     CMdlTree *aTree = new CMdlTree();
	     aTree->initialize(pGBM, cNodes);
	     aTree->write_dots(sFileDot);
	     mLog.info( aTree->toString() );

	     std::string sFilePrd="logs/prdMdlTree"+std::to_string(iCtr);
	     aTree->predictAndSave( cTrain, adY, adX, sFilePrd );

	     // [] Forest insertion
	     dMdlFrst->insert(iCtr,aTree);
	   }

	   mLog.info( "dMdlFrst %s",dMdlFrst->toString());
	   std::string sFilePrdFrst="logs/prdMdlFrst";
	   dMdlFrst->predictAndSave( cTrain, cTrees, adY, adX, sFilePrdFrst );
	   dMdlFrst->predictAndSave2( cTrain, cTrees, adY, adX, sFilePrdFrst );
}



void r2xTst::t2b_gbm_run() {

  int cRows=4;
  int cCols=2;
  int cTrees=10; // number of trees to build
  int cTrain = 3;

  double *adY = new double[cRows];
  double *adOffset = NULL;
  double *adX = new double[cRows*cCols];
  int *aiXOrder = new int[cRows*cCols];
  double *adWeight = new double[cRows];
  double *adMisc = NULL;

  int *acVarClasses = new int[cCols];
  int *alMonotoneVar = new int[cCols];
  char pszFamily [] = "gaussian";
  int cLeavesDepth = 1;
  int cMinObsInNode = 1;
  double dShrinkage = 0.5;
  double dBagFraction = 0.5;

  CDataset *pData = new CDataset();
  CDistribution *pDist = NULL;


  // [] initialize values
  for(int iCtr=0; iCtr<(cRows*cCols); iCtr++) {

  }
  for(int cCtr=0; cCtr<cCols; cCtr++) {
    acVarClasses[cCols] = 0;
    alMonotoneVar[cCols] = 0;
  }
  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;

  adX[0] = 0.1;
  adX[1] = 0.2;
  adX[2] = 0.3;
  adX[3] = 0.4;
  adX[4] = 0.4;
  adX[5] = 0.3;
  adX[6] = 0.2;
  adX[7] = 0.1;

  aiXOrder[0] = 0;
  aiXOrder[1] = 1;
  aiXOrder[2] = 2;
  aiXOrder[3] = 3;
  aiXOrder[4] = 3;
  aiXOrder[5] = 2;
  aiXOrder[6] = 1;
  aiXOrder[7] = 0;



  // initialize some things
   gbm_setup(adY,
                 adOffset,
		 adX,
		 aiXOrder,
		 adWeight,
		 adMisc,
		 cRows,
		 cCols,
		 acVarClasses,
		 alMonotoneVar,
		 pszFamily,
		 cTrees,
		 cLeavesDepth,
		 cMinObsInNode,
		 dShrinkage,
		 dBagFraction,
		 cTrain,
		 pData,
		 pDist);


   // []
   std::cout << " initialize " << std::endl;
   double dLambda = 0.1;
   double dNumTrain = cRows;
   //double dBagFraction =0.75;
   //unsigned long cLeaveDepth = 3;
   //unsigned long cMinsObsInNode = 3;
   CGBM *pGBM = new CGBM();
   pGBM->Initialize(pData,
		    pDist,
		    dLambda,
		    dNumTrain,
		    dBagFraction,
		    cLeavesDepth,
		    cMinObsInNode);
   std::cout << " fInitialized " << pGBM->fInitialized <<  std::endl;

   //[]
   int cNodes = 0;
   double *adF = new double[cRows]; // predictions
   for(int iCtr=0; iCtr<cTrees; iCtr++) {
     std::cout << " iCtr " << iCtr << std::endl;
     double dTrainError = 0.0;
     double dValidError = 0.0;
     double dOOBagImprove = 0.0;

     pGBM->iterate(adF,
                 dTrainError,
                 dValidError,
                 dOOBagImprove,
                 cNodes);

     std::vector<double> vdF(adF,adF+sizeof adF / sizeof adF[0]);
     std::string         sdF(vdF.begin(), vdF.end());
     std::cout << "sdF"<< sdF << std::endl;
     printf("cNodes %d r%f s%f b%f\n",cNodes, dTrainError, dValidError, dOOBagImprove );
     printf("==============================================\n");

   }

}


/*
 * data directly parse from cpp classes
 * ../../../../../../db/rk/letor4/mq7b/Fold1/train.txt
 * 42158
 * 46
 *
 * std::string sDbNameOriginal
 * 10
 * 4
 * 	// [] Data :: Constants
 * 	int cRows           = 42158;
 * 	int cCols           = 46;
 * 	std::string sDbNameOriginal="../../../../../../db/rk/letor4/mq7b/Fold1/train.txt";
 *
 */
void r2xTst::t2c_gbf(gbf_settings cfg) {

	//[]
	mLogRoot.alert(" %d x %d ",cfg.m_crows,cfg.m_ccols);
	mLogRoot.alert(" %s %s",cfg.m_dbloc.c_str(),cfg.m_dbname.c_str());
	mLogRoot.alert(" %s %s",cfg.m_result_loc.c_str(), cfg.m_log_loc.c_str());

	std::string sLgMdlTreeNameDot=cfg.m_resultTreeDot;// "logs/dotMdlTree";
	std::string sLgPrdTree=cfg.m_resultTreePred; //"logs/prdMdlTree";
	std::string sLgPrdFrst=cfg.m_resultForestPred; //"logs/prdMdlFrst";

	// [] Data :: Constants
	int cRows           = cfg.m_crows;
	int cCols           = cfg.m_ccols;
	std::string sDbNameOriginal = cfg.m_dbloc;//"../dat/letor1/r1a.txt";

	int cTrain          = cRows;
	double dBagFraction = 1.0;

	// [] Trees ::
	int cTrees          = 100; // number of trees to build
	int cLeavesDepth    = 10;
	int cMinObsInNode   = 1;
	double dShrinkage   = 0.5;
	double dLambda      = 0.1; // Is dLambda the same as dShrinkage





	// [] Data :: Structures
	char pszFamily []    = "gaussian";
	CDistribution *pDist = NULL;

	CDataset *pData      = new CDataset();

	double *adY          = new double[cRows];
	double *adWeight     = new double[cRows];

	double *adX          = new double[cRows*cCols];
	int *aiXOrder        = new int[cRows*cCols];

	double *adQ          = new double[cRows];

	double *adOffset = NULL;
	double *adMisc   = NULL;

	int *acVarClasses   = new int[cCols];
	int *alMonotoneVar  = new int[cCols];

	// [] initialize values
	// for(int cCtr=0; cCtr<cCols; cCtr++) { acVarClasses[cCtr] = 0; alMonotoneVar[cCtr] = 0; }
	mGbmUtl->utl_init_array_to_zeros(acVarClasses, cCols);
	mGbmUtl->utl_init_array_to_zeros(alMonotoneVar, cCols);


	// [] load the dataset
	int iLenObs=mGbmUtl->db_num_lines(sDbNameOriginal);
	int iLenFeat=mGbmUtl->db_num_features(sDbNameOriginal);
	mLog.info(" sDbName %s ,iLenObs %d ,iLenFeat %d", sDbNameOriginal.c_str(), iLenObs, iLenFeat);
	mGbmUtl->db_fromFile(sDbNameOriginal, iLenObs, iLenFeat, adY, adQ, adWeight, adX);
	//mGbmUtl->db_init204(adX, adY, adWeight);

	mGbmUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	mGbmUtl->db_print_array_log(adX, cRows*cCols);
	mGbmUtl->db_print_order_log(aiXOrder, cRows*cCols);
	mGbmUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);
	// mGbmUtl->db_save(cRows, cCols, adX, adY, adQryId, sDbName);

	// initialize some things
	gbm_setup(adY,
			adOffset,
			adX,
			aiXOrder,
			adWeight,
			adMisc,
			cRows,
			cCols,
			acVarClasses,
			alMonotoneVar,
			pszFamily,
			cTrees,
			cLeavesDepth,
			cMinObsInNode,
			dShrinkage,
			dBagFraction,
			cTrain,
			pData,
			pDist);

	   // []
	   mLog.info(" initialize ");
	   double dNumTrain = cTrain;
	   //double dBagFraction =0.75;
	   //unsigned long cLeaveDepth = 3;
	   //unsigned long cMinsObsInNode = 3;
	   CGBM *pGBM = new CGBM();
	   pGBM->Initialize(pData,
			    pDist,
			    dLambda,
			    dNumTrain,
			    dBagFraction,
			    cLeavesDepth,
			    cMinObsInNode);


	   //[]
	   int cNodes = 0;
	   double *adF = new double[cRows]; // predictions
	   mGbmUtl->utl_init_array_to_zeros(adF, cRows);
	   //for(int iCtr=0; iCtr<cRows; iCtr++) {		   adF[iCtr] = 0.0;	   }

	   CMdlForest *dMdlFrst = new CMdlForest();
	   dMdlFrst->initialize(cTrees);
	   for(int iCtr=0; iCtr<cTrees; iCtr++) {
		   mLog.info("iCtr %d", iCtr);

	     double dTrainError = 0.0;
	     double dValidError = 0.0;
	     double dOOBagImprove = 0.0;

	     pGBM->iterate(adF,
	                 dTrainError,
	                 dValidError,
	                 dOOBagImprove,
	                 cNodes);

	     std::vector<double> vdF(adF,adF+sizeof adF / sizeof adF[0]);
	     std::string         sdF(vdF.begin(), vdF.end());
	     //std::cout << "sdF"<< sdF << std::endl;
	     mLog.info("cNodes %d r %f s %f b %f",cNodes, dTrainError, dValidError, dOOBagImprove );
	     mLog.info("adF %s", sdF.c_str());
	     pGBM->PrintTree();
	     mLog.info("==============================================");


		 // []
	     std::string sFileDot=sLgMdlTreeNameDot+std::to_string(iCtr);
	     mLog.info(" dotMdlTree %s ", sFileDot.c_str());
	     CMdlTree *aTree = new CMdlTree();
	     aTree->initialize(pGBM, cNodes);
	     aTree->write_dots(sFileDot);
	     mLog.info( aTree->toString() );

	     std::string sFilePrd=sLgPrdTree+std::to_string(iCtr);
	     aTree->predictAndSave( cTrain, adY, adX, sFilePrd );

	     // [] Forest insertion
	     dMdlFrst->insert(iCtr,aTree);
	   }

	   mLogRoot.alert(" sLgPrdFrst %s ",sLgPrdFrst.c_str() );
	   //mLog.info( "dMdlFrst %s",dMdlFrst->toString());
	   dMdlFrst->predictAndSave( cTrain, cTrees, adY, adX,sLgPrdFrst );
	   //dMdlFrst->predictAndSave2( cTrain, cTrees, adY, adX, sLgPrdFrst );


}



void r2xTst::t3a_transfer() { }
void r2xTst::t4a_print() { }


void r2xTst::t5a_plot() {

	/*
	// [] config4cpp
	setlocale(LC_ALL, "");

	const char *    logDir;
	int             logLevel, timeout;
	const char *    scope = "foo_srv";
	config4cpp::Configuration * cfg = config4cpp::Configuration::create();
	try {
	  //cfg->parse(getenv("FOO_CONFIG"));
	  cfg->parse("conf/c1.cfg");
	  logDir   = cfg->lookupString(scope, "log.dir");
	  logLevel = cfg->lookupInt(scope, "log.level");
	  timeout  = cfg->lookupDurationSeconds(scope, "timeout");
	  std::cout<< logDir << std::endl;
	  std::cout<< logLevel << std::endl;
	  std::cout<< timeout << std::endl;
	} catch(const config4cpp::ConfigurationException & ex) {
	  cout << ex.c_str() << endl;
	}

	cfg->destroy();
	*/
}

