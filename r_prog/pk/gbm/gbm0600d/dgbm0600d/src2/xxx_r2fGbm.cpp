
#include<string>
#include<vector>
#include<iostream>

#include "gbm.h"
#include "gbm_engine.h"
#include "gaussian.h"

#include "f101_info_debug.h"

#include "xxx_r2bGbm.h"
#include "xxx_r2cGbm.h"
#include "xxx_r2dGbm.h"

int main() {

  // [] Data :: Constants
  int cRows  = 4;
  int cCols  = 6;
  int cTrain = 3;
  double dBagFraction = 0.5;

  // [] Trees ::
  int cTrees          = 1; // number of trees to build
  int cLeavesDepth    = 1;
  int cMinObsInNode   = 1;
  double dShrinkage   = 0.5;

  // [] Data :: Structures
  char pszFamily []   = "gaussian";
  CDistribution *pDist = NULL;

  CDataset *pData      = new CDataset();

  double *adY      = new double[cRows];
  double *adWeight = new double[cRows];

  double *adX      = new double[cRows*cCols];
  int *aiXOrder    = new int[cRows*cCols];

  double *adOffset = NULL;
  double *adMisc   = NULL;

  int *acVarClasses   = new int[cCols];
  int *alMonotoneVar  = new int[cCols];



  // [] initialize values
  for(int cCtr=0; cCtr<cCols; cCtr++) {
    acVarClasses[cCtr] = 0;
    alMonotoneVar[cCtr] = 0;
  }
  db_init46(adX, adY, adWeight);  
  db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);



  
  
  
  return 0;
}
