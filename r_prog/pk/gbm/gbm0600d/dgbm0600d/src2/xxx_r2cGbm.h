
#include<string>
#include<vector>
#include<iostream>
#include<iterator>
#include<numeric>
#include<algorithm>

/*
void db_print(double *dX, int iRow, int iCol) {

  for(int i=0; i<iCol; i++) {
    for(int j=0; j<iRow; j++) {      
      printf(" %f ", dX[i*iRow+j]);
    }
    printf("\n");    
  }
  
}
*/

void db_sort_mat_o_idx(double *dX, int iRow, int iCol, int* piOutIdx) {

  //printf("iRow:%d iCol:%d \n",iRow,iCol);
  for(int i=0; i<iCol; i++) {
    // [] get an array of values
    double* dTmp = new double[iRow];
    for(int j=0; j<iRow; j++) {
    dTmp[j] = dX[i*iRow+j];
    }

    // [] do sorting
    int* piTmpIdx = new int[iRow];
    db_sort_array(dTmp, iRow,  piTmpIdx );
    // [] copy to output
    for(int j=0; j<iRow; j++) {
      piOutIdx[i*iRow+j]=piTmpIdx[j];
    }
  }

  
}
