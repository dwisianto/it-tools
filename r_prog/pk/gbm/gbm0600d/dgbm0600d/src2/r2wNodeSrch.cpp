
#include <iostream>
#include "node_factory.h"
#include "node_search.h"
#include "r2wNodeSrch.h"

using namespace std;

class r2wTst {
public:
	void t1a_node_factory();
	void t2a_node_search();
};



void r2wTst::t1a_node_factory() {
	cout<< "t1a" << endl;

	unsigned long cDepth = 3;
	CNodeFactory *nf = new CNodeFactory();
	nf->Initialize(cDepth);
	CNodeTerminal *nt = nf->GetNewNodeTerminal();

}

void r2wTst::t2a_node_search() {
	cout<< "t2a" << endl;

	unsigned long cMinObsInNode = 1;
	CNodeSearch * ns = new 	CNodeSearch();
	ns->Initialize(cMinObsInNode);

    unsigned long iWhichVar = 1;
    long cCurrentVarClasses = 0;
	ns->ResetForNewVar( iWhichVar, cCurrentVarClasses);
	ns->WrapUpCurrentVariable();


}

int main() {

   cout << "Hello \n";
   r2wTst* r2w = new r2wTst();
   r2w->t1a_node_factory();
   r2w->t2a_node_search();
   cout << "Bye\n";

   return 0;
}
