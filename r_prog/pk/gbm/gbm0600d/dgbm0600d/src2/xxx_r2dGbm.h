
void db_init46(double * adX, double * adY, double * adWeight) {

  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;
  
   adX[0] = 0.11;
   adX[1] = 0.13;
   adX[2] = 0.12;
   adX[3] = 0.14;
   
   adX[4] = 0.28;
   adX[5] = 0.26;
   adX[6] = 0.27;
   adX[7] = 0.29;
   
   adX[8] = 0.31;
   adX[9] = 0.33;
   adX[10] = 0.34;
   adX[11] = 0.32;

   adX[12] = 0.46;
   adX[13] = 0.48;
   adX[14] = 0.49;
   adX[15] = 0.47;

   adX[16] = 0.51;
   adX[17] = 0.53;
   adX[18] = 0.54;
   adX[19] = 0.52;

   adX[20] = 0.66;
   adX[21] = 0.68;
   adX[22] = 0.69;
   adX[23] = 0.67;
   
}


void db_init64(double * adX, double * adY, double * adWeight) {

  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;
  adY[4] = 1;
  adY[5] = 0;  

  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;
  adWeight[4] = 1;
  adWeight[5] = 1;
  
   adX[0] = 0.11;
   adX[1] = 0.13;
   adX[2] = 0.12;
   adX[3] = 0.14;
   adX[4] = 0.18;
   adX[5] = 0.16;
   
   adX[6] = 0.27;
   adX[7] = 0.29;
   adX[8] = 0.21;
   adX[9] = 0.23;
   adX[10] = 0.24;
   adX[11] = 0.22;

   adX[12] = 0.36;
   adX[13] = 0.38;
   adX[14] = 0.39;
   adX[15] = 0.37;
   adX[16] = 0.51;
   adX[17] = 0.53;
   
   adX[18] = 0.44;
   adX[19] = 0.42;
   adX[20] = 0.46;
   adX[21] = 0.48;
   adX[22] = 0.49;
   adX[23] = 0.47;
   
}


void db_init104(double * adX, double * adY, double * adWeight) {


  adY[0] = 1;
  adY[1] = 0;
  adY[2] = 1;
  adY[3] = 0;
  adY[4] = 1;
  adY[5] = 0;
  adY[6] = 1;
  adY[7] = 0;
  adY[8] = 1;
  adY[9] = 0;
  
  adWeight[0] = 1;
  adWeight[1] = 1;
  adWeight[2] = 1;
  adWeight[3] = 1;
  adWeight[4] = 1;
  adWeight[5] = 1;
  adWeight[6] = 1;
  adWeight[7] = 1;
  adWeight[8] = 1;
  adWeight[9] = 1;

  for(int i=0; i<40; i++) {
    adX[i] = (rand () % 100)*1.0/100;
    printf(" %f ",adX[i]);
  }
  
}

void db_init204(double * adX, double * adY, double * adWeight) {

  for(int i=0; i<10; i++) {
    adY[i] = 1;
    adWeight[i] = 1;
  }
  for(int i=10; i<20; i++) {
    adY[i] = 0;
    adWeight[i] = 1;
  }

  for(int i=0; i<80; i++) {
    adX[i] = (rand () % 100)*1.0/100;
  }
  
}
