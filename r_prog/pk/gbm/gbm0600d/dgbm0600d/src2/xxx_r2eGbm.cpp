
#include<string>
#include<vector>
#include<iostream>

#include "gbm.h"
#include "gbm_engine.h"
#include "gaussian.h"
#include "f101_info_debug.h"

#include "xxx_r2bGbm.h"
#include "xxx_r2cGbm.h"
#include "xxx_r2dGbm.h"




int main() {

  // [] Data :: Constants
  int cRows  = 4;
  int cCols  = 6;
  int cTrain = cRows-1;
  double dBagFraction = 0.8;

  // [] Trees ::
  int cTrees          = 1; // number of trees to build
  int cLeavesDepth    = 3;
  int cMinObsInNode   = 1;
  double dShrinkage   = 0.5;

  // [] Data :: Structures
  char pszFamily []   = "gaussian";
  CDistribution *pDist = NULL;

  CDataset *pData      = new CDataset();

  double *adY      = new double[cRows];
  double *adWeight = new double[cRows];

  double *adX      = new double[cRows*cCols];
  int *aiXOrder    = new int[cRows*cCols];

  double *adOffset = NULL;
  double *adMisc   = NULL;

  int *acVarClasses   = new int[cCols];
  int *alMonotoneVar  = new int[cCols];


  // [] initialize values
  for(int cCtr=0; cCtr<cCols; cCtr++) {
    acVarClasses[cCtr] = 0;
    alMonotoneVar[cCtr] = 0;
  }
  db_init46(adX, adY, adWeight);
  db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);

  // initialize some things
   gbm_setup(adY,
                 adOffset,
		 adX,
		 aiXOrder,
		 adWeight,
		 adMisc,
		 cRows,
		 cCols,
		 acVarClasses,
		 alMonotoneVar,
		 pszFamily,
		 cTrees,
		 cLeavesDepth,
		 cMinObsInNode,
		 dShrinkage,
		 dBagFraction,
		 cTrain,
		 pData,
		 pDist);


   // []
   std::cout << " initialize " << std::endl;
   double dLambda = 0.1;
   double dNumTrain = cRows;
   //double dBagFraction =0.75;
   //unsigned long cLeaveDepth = 3;
   //unsigned long cMinsObsInNode = 3;
   CGBM *pGBM = new CGBM();
   pGBM->Initialize(pData,
		    pDist,
		    dLambda,
		    dNumTrain,
		    dBagFraction,
		    cLeavesDepth,
		    cMinObsInNode);
   std::cout << " main::fInitialized " << pGBM->fInitialized <<  std::endl;

   //[]
   int cNodes = 0;
   double *adF = new double[cRows]; // predictions
   for(int iCtr=0; iCtr<cTrees; iCtr++) {
     std::cout << " iCtr " << iCtr << std::endl;
     double dTrainError = 0.0;
     double dValidError = 0.0;
     double dOOBagImprove = 0.0;

     pGBM->iterate(adF,
                 dTrainError,
                 dValidError,
                 dOOBagImprove,
                 cNodes);

     //std::vector<double> vdF(adF,adF+sizeof adF / sizeof adF[0]);
     //std::string         sdF(vdF.begin(), vdF.end());
     //std::cout << "sdF"<< sdF << std::endl;
     printf("cNodes %d r%f s%f b%f\n",cNodes, dTrainError, dValidError, dOOBagImprove );
     printf("==============================================\n");

   }
   pGBM->PrintTree();


   std::cout << "Bye !\n";
   return 0;
}
