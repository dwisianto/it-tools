
#include "xxx_r2bGbm.h"
#include "xxx_r2cGbm.h"
#include "xxx_r2dGbm.h"

int main() {

   std::cout << "Hello !\n";

   int cRows=4;
   int cCols=6;

   double *adX = new double[cRows*cCols];
   double *adY = new double[cRows];
   db_init46( adX, adY);
   
   //db_print(adX, cRows, cCols);
   db_print_array(adX,cRows*cCols);

   // sort and get index
   int *aiXOrder = new int[cRows*cCols];
   db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
   db_print_array(aiXOrder,cRows*cCols);
   
   std::cout << "Bye !\n";
   return 0;     
}
