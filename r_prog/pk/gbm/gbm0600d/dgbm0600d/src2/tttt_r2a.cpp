
#include <iostream>
#include "node_factory.h"
#include "node_search.h"

using namespace std;

class c2a {
public:
	void t1a_node_factory();
	void t2a_node_search();
};



void c2a::t1a_node_factory() {
	cout<< "t1a" << endl;

	unsigned long cDepth = 3;
	CNodeFactory *nf = new CNodeFactory();
	nf->Initialize(cDepth);
	CNodeTerminal *nt = nf->GetNewNodeTerminal();

}

void c2a::t2a_node_search() {
	cout<< "t2a" << endl;

	unsigned long cMinObsInNode = 1;
	CNodeSearch * ns = new 	CNodeSearch();
	ns->Initialize(cMinObsInNode);

    unsigned long iWhichVar = 1;
    long cCurrentVarClasses = 0;
	ns->ResetForNewVar( iWhichVar, cCurrentVarClasses);
	ns->WrapUpCurrentVariable();


}

int main() {

   cout << "Hello \n";
   c2a* cc = new c2a();
   cc->t1a_node_factory();
   cc->t2a_node_search();
   cout << "Bye\n";

   return 0;
}
