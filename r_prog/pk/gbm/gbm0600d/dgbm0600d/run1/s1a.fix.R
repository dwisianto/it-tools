

l_rsrc='/media/dsm/K3Attic/KBb/it-tools/r_prog/pk/gbm/gbm0600d/dgbm0600d/Rsrc/'
source(file.path(l_rsrc,'dgbm.train.R'))
source(file.path(l_rsrc,'dgbm.plot.cont2d.R'))
source(file.path(l_rsrc,'dgbm.predict.R'))
source(file.path(l_rsrc,'dgbm.pretty.tree.R'))

#detach(package:dgbm0600d)
library(dgbm0600d)




X1 <- c(0.6,0.4,0.2,0.8,1.0)
X2 <- c(0.3,0.9,0.5,0.1,0.7)
Y <- c(1,2,3,4,5)
data <- data.frame(Y=Y,X1=X1,X2=X2)

# fit initial model
#weights=w,
#var.monotone=c(0,0,0,0,0,0), # -1: monotone decrease, +1: monotone increase, 0: no monotone restrictions
gbm1 <- dgbm.grow(Y~X1+X2,           # formula
                  data=data,                   # dataset
                  distribution="gaussian",     # bernoulli, adaboost, gaussian, poisson, and coxph available
                  n.trees=10,                 # number of trees
                  shrinkage=0.005,             # shrinkage or learning rate, 0.001 to 0.1 usually work
                  interaction.depth=3,         # 1: additive model, 2: two-way interactions, etc
                  bag.fraction = 1.0,          # subsampling fraction, 0.5 is probably best
                  train.fraction = 1.0,        # fraction of data for training, first train.fraction*N used for training
                  n.minobsinnode = 1,         # minimum number of obs needed in each node
                  keep.data=T)

