
extern "C" {
  
#include <R.h>
#include <Rdefines.h>
#include <stdio.h>
  
  SEXP f_hw( ) {
    printf("Hello World!\n");
    return(R_NilValue);
  }

  /**
   **/
  SEXP f_hw_arg_integer(
    SEXP rcRows,
    SEXP rcCols  
  ) {
    printf("Hello World Argument !\n");
    
    int cRows = INTEGER(rcRows)[0];
    int cCols = INTEGER(rcCols)[0];
    
    printf("rcCols %d rcRows %d \n",cCols,cRows);
    return(R_NilValue);
  }
  

  SEXP f_hw_param(
  SEXP radY // outcome or response
  ) {
    printf("Hello World Param!\n");
    double *pY = REAL(radY);
    /*
    while( *pY != R_NilValue) {
           printf("%f\n",*pY);
           pY++;
    }
    */

    return(R_NilValue);
  }

  /**
   **/
  SEXP f_hw_ret_integer(
    SEXP rcRows,
    SEXP rcCols  
  ) {
    printf("Hello World Argument !\n");
    
    int cRows = INTEGER(rcRows)[0];
    int cCols = INTEGER(rcCols)[0];

    SEXP result = PROTECT(allocVector(REALSXP, 1));
    REAL(result)[0] = cRows+cCols;
    UNPROTECT(1);
    
    printf("rcCols %d rcRows %d \n",cCols,cRows);
    return(result);
  }



  /**
   * Random Number generator
   **/
  SEXP f_hw_rnd(){

   SEXP output;
   
   GetRNGstate();
   
   PROTECT(output = allocVector(INTSXP, 1));
   INTEGER(output)[0] = rand() % 50;
   PutRNGstate();
   UNPROTECT(1);
   
   return(output);
 }
  
} // end extern "C"
