/*
 * f711_model_tree.h
 *
 *  Created on: Apr 25, 2017
 *      Author: dsm
 */



#include "f711_model_tree.h"


CMdlTree::CMdlTree()
{
}

CMdlTree::~CMdlTree()
{
}


HRESULT CMdlTree::initialize( CGBM *pGBM, int iSizeNode  ) {

	miSizeNode = iSizeNode;
    //miSizeNode = new int[iSizeNode];
	maiSplitVar = new int[iSizeNode];
	madSplitPoint = new double[iSizeNode];
	maiLeftNode = new int[iSizeNode];
	maiRightNode  = new int[iSizeNode];
	maiMissingNode  = new int[iSizeNode];
	madErrorReduction = new double[iSizeNode];
	madWeight = new double[iSizeNode];
	VEC_VEC_CATEGORIES mVecSplitCodes (iSizeNode);
	miCatSplitsOld = 0;

	pGBM->TransferTreeToRList(maiSplitVar,
			madSplitPoint,
			maiLeftNode,
			maiRightNode,
			maiMissingNode,
			madErrorReduction,
			madWeight,
			mVecSplitCodes,
			miCatSplitsOld);

	mLog.info("cCatSplitsOld %d ",miCatSplitsOld);
	pUtil->log_array_int("aiSplitVar",maiSplitVar, miSizeNode);
	pUtil->log_array_double("splitPoint",madSplitPoint, miSizeNode);
	pUtil->log_array_int("aiLeftNode",maiLeftNode, miSizeNode);
	pUtil->log_array_int("aiRightNode",maiRightNode, miSizeNode);
	pUtil->log_array_int("aiMissingNode",maiMissingNode, miSizeNode);
	pUtil->log_array_double("adErrorReduction",madErrorReduction, miSizeNode);
	pUtil->log_array_double("adWeight",madWeight,  miSizeNode);

}

const char* CMdlTree::toString( ) {

    std::string sOut="\n";
    sOut += "\t iCtr \t Weight \t Error \t Var \t Val \t Left \t Right \t Missing \n";
    for(int iCtr=0; iCtr<miSizeNode; iCtr++) {
    	sOut = sOut + "\t" + std::to_string(iCtr) + "\t"+ std::to_string(madWeight[iCtr]);
    	sOut = sOut + "\t" + std::to_string(madErrorReduction[iCtr]);
    	sOut = sOut + "\t" + std::to_string(maiSplitVar[iCtr]) + "\t" + std::to_string(madSplitPoint[iCtr]);
    	sOut = sOut + "\t" + std::to_string(maiLeftNode[iCtr]) + "\t" + std::to_string(maiRightNode[iCtr]);
    	sOut = sOut + "\t" + std::to_string(maiMissingNode[iCtr]);
    	sOut = sOut + "\n";
    }

    return sOut.c_str();
}


HRESULT CMdlTree::write_dots(std::string sOutDot) {

	mLog.info("write_dots");

	std::string sLabel = "";
	std::string sOut="\n";
	sOut += "digraph tt {\n";
	//sOut += "n0";
    for(int iCtr=0; iCtr<miSizeNode; iCtr++) {

    	if( maiSplitVar[iCtr] == -1) {
    		sLabel = std::to_string(madSplitPoint[iCtr]); // "t" + std::to_string(iCtr);
    		sOut += "n" + std::to_string(iCtr) + "[shape=\"box\",label=\""+ sLabel +"\"]\n";
    	} else {
    		//sLabel = "n" + std::to_string(iCtr);
    		sLabel = "v" + std::to_string(maiSplitVar[iCtr]+1) + " > " + std::to_string(madSplitPoint[iCtr])+" ";
    		sLabel = sLabel + "(Err=" + std::to_string(madErrorReduction[iCtr]) + ")";
    		sOut += "n" + std::to_string(iCtr) + "[label=\""+ sLabel + "\"]\n";
    		if( maiRightNode[iCtr] != -1) {
    			sLabel = "[label=\"R"+std::to_string((int) madWeight[maiRightNode[iCtr]])+"\"]";
    			sOut +="n" + std::to_string(iCtr)+"->"+"n" + std::to_string(maiRightNode[iCtr])+sLabel+"\n";
    		}
    		if( maiLeftNode[iCtr] != -1) {
    			sLabel = "[label=\"L"+std::to_string((int) madWeight[maiLeftNode[iCtr]])+"\"]";
    			sOut +="n" + std::to_string(iCtr)+"->"+"n" + std::to_string(maiLeftNode[iCtr])+sLabel+"\n";
    		}
    		if( maiMissingNode[iCtr] != -1) {
    			sLabel = "[label=\"M"+std::to_string((int) madWeight[maiMissingNode[iCtr]])+"\" style=dotted ]";
    			sOut +="n" + std::to_string(iCtr)+"->"+"n" + std::to_string(maiMissingNode[iCtr])+sLabel+"\n";
    		}
    	}

    }
	sOut += "\n}";

	std::ofstream myfile;
	myfile.open (sOutDot);
	myfile << sOut;
	myfile.close();
	mLog.info("%s",sOut.c_str());
}

HRESULT CMdlTree::predict( int iSizeObs, double *adX, double *adF ) {

    //unsigned long hr = 0;


    int cRows = iSizeObs;
    //int cTrees = iSizeTree;

    //SEXP rThisTree = NULL;
    /*
    int *aiSplitVar = NULL;
    double *adSplitCode = NULL;
    int *aiLeftNode = NULL;
    int *aiRightNode = NULL;
    int *aiMissingNode = NULL;

    int iCatSplitIndicator = 0;
    */

    // []
    int iObs = 0;
    int iCurrentNode = 0;
    //double* adPredF = new double[cRows];
    for(iObs=0; iObs<cRows; iObs++) {
        adF[iObs] = 0;
    }

    //int iTree = 0;
    double dX = 0.0;
    //for(iTree=0; iTree<cTrees; iTree++) {
        //rThisTree     = VECTOR_ELT(rTrees,iTree);
        //aiSplitVar    = this->maiSplitVar;
        //adSplitCode   = this->madSplitPoint;
        //aiLeftNode    = this->maiLeftNode;
        //aiRightNode   = this->maiRightNode;
        //aiMissingNode = this->maiMissingNode;
        for(iObs=0; iObs<cRows; iObs++) {
        	iCurrentNode = 0;
        	while(maiSplitVar[iCurrentNode] != -1) {
        		dX = adX[maiSplitVar[iCurrentNode]*cRows + iObs];
        		if(dX < madSplitPoint[iCurrentNode]) {
        			iCurrentNode = maiLeftNode[iCurrentNode];
        		} else {
        			iCurrentNode = maiRightNode[iCurrentNode];
        		}
        	}
        	adF[iObs] += madSplitPoint[iCurrentNode]; // add the prediction
        } // iObs
    //} // iTree


    // []
    HRESULT hr = S_OK;
Cleanup:
    return hr;
Error:
    goto Cleanup;
}

HRESULT CMdlTree::predictAndSave( int iSizeObs, double *adY, double *adX, std::string sOutFile ) {

    double *adPred = new double[iSizeObs];
    predict( iSizeObs, adX, adPred);

    std::string sOut="";
    for(int i=0; i<iSizeObs; i++) {
    	sOut = sOut + "\t" + std::to_string(i);
    	sOut = sOut + "\t" + std::to_string(adPred[i]);
    	sOut = sOut + "\t" + std::to_string(adY[i]);
    	sOut = sOut + "\n";
    }
    sOut+="\n";

	std::ofstream myfile;
	myfile.open (sOutFile);
	myfile << sOut;
	myfile.close();
	mLog.info("%s",sOut.c_str());

}

