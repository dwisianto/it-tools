//------------------------------------------------------------------------------
//  GBM by Greg Ridgeway  Copyright (C) 2003
//
//  File:       node_factory.h
//
//  License:    GNU GPL (version 2 or later)
//
//  Contents:   manager for allocation and destruction of all nodes
//
//  Owner:      gregr@rand.org
//
//  History:    3/26/2001   gregr created
//              2/14/2003   gregr: adapted for R implementation
//
//------------------------------------------------------------------------------

#ifndef NODEFACTORY_H
#define NODEFACTORY_H

#include <stack>
#include <list>
#include "node_terminal.h"
#include "node_continuous.h"
#include "node_categorical.h"

#define NODEFACTORY_NODE_RESERVE ((unsigned long)100)

using namespace std;

class CNodeFactory
{
public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("NodeFact"));
    CNodeFactory();
    ~CNodeFactory();

    HRESULT Initialize(unsigned long cDepth);
    CNodeTerminal* GetNewNodeTerminal();
    CNodeContinuous* GetNewNodeContinuous();
    CNodeCategorical* GetNewNodeCategorical();
    HRESULT RecycleNode(CNodeTerminal *pNode);
    HRESULT RecycleNode(CNodeContinuous *pNode);
    HRESULT RecycleNode(CNodeCategorical *pNode);

private:
    stack<PCNodeTerminal> TerminalStack;
    stack<PCNodeContinuous> ContinuousStack;
    stack<PCNodeCategorical> CategoricalStack;

    CNodeTerminal* pNodeTerminalTemp;
    CNodeContinuous* pNodeContinuousTemp;
    CNodeCategorical* pNodeCategoricalTemp;

    CNodeTerminal aBlockTerminal[NODEFACTORY_NODE_RESERVE];
    CNodeContinuous aBlockContinuous[NODEFACTORY_NODE_RESERVE];
    CNodeCategorical aBlockCategorical[NODEFACTORY_NODE_RESERVE];
};

#endif // NODEFACTORY_H
