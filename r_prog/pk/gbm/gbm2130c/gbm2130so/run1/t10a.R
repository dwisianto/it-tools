library(parallel)
library(gbm2130so)
#setwd('/media/dsm/K3Attic/KBb/it-tools/r_prog/pk/gbm/gbm2130c/dgbm2130so/run1')
#cat(getwd())
#detach(package:dgbm0600d)
#library(dgbm0600d)

N <- 10
X1 <- runif(N)
X2 <- 2*runif(N)
Y <- X1*1.5 + 2 * (X2*.5) 
data <- data.frame(Y=Y,X1=X1,X2=X2)


# fit initial model
#weights=w,
#var.monotone=c(0,0,0,0,0,0), # -1: monotone decrease, +1: monotone increase, 0: no monotone restrictions
gbm1 <- gbm(Y~X1+X2,      # formula
                    data=data,         # dataset
                    distribution='gaussian', # loss function: gaussian
                    n.trees=2,            # number of trees
                    shrinkage=0.005,         # learning rate
                    interaction.depth=3,     # number per splits per tree
                    bag.fraction = 1.0,      # subsampling fraction
                    train.fraction = 1,      # fraction of data for training
                    n.minobsinnode = 1,     # minimum number of obs for split
                    keep.data=TRUE,          # store copy of input data in model
                    cv.folds=1,              # number of cross validation folds
                    verbose = FALSE,         # don't print progress
                    n.cores = 1)             # use a single core (to prevent possible problems caused by wronly detecting cores)


#cfg.numTrees=2
#dat.pred = predict.gbm(gbm1, data, n.trees=cfg.numTrees)
#dat.pred.max = max(dat.pred)
#dat.pred <- dat.pred/dat.pred.max

#
#pretty.gbm.tree(gbm1, i.tree=1)
#print(rbind(gbm1$train.error,gbm1$valid.error))

#plot.gbm(gbm1)
#plot.gbm(gbm1, i.var=c(1,2))
#plot.gbm(gbm1, i.var=c(1,2), n.trees=1)

#dgbm.plot.cont2d(gbm1,i.var=c(1,2), n.trees=1)

