// GBM by Greg Ridgeway  Copyright (C) 2003

extern "C" {

#include <R.h>
#include <Rdefines.h>
#include <Rinternals.h>

// https://www.r-bloggers.com/three-ways-to-call-cc-from-r/  
SEXP dgbm_hw(SEXP x) {
  Rprintf(" dgbm_hw \n");
  // Doubles the value of the first integer element of the SEXP input
  SEXP result;
  PROTECT(result = NEW_INTEGER(1)); // i.e., a scalar quantity
  INTEGER(result)[0] = INTEGER(x)[0] * 2;
  UNPROTECT(1); // Release the one item that was protected
  return result;
}


    
} // end extern "C"
