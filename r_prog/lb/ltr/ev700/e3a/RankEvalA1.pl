#!
# author: Jun Xu and Tie-Yan Liu
# modified by Jun Xu, March 3, 2009 (for Letor 4.0)
use strict;

#hash table for Precision@N and MAP
my %hsPrecisionRel = ("2", 1,
                      "1", 1,
                      "0", 0
                );
#modified by Jun Xu, March 3, 2009
# for Letor 4.0. only output top 10 precision and ndcg
my $iMaxPosition = 10;

my $argc = $#ARGV+1;
if($argc != 4)
{
		print "Invalid command line.\n";
		print "Usage: perl Eval.pl argv[1] argv[2] argv[3] argv[4]\n";
		print "argv[1]: feature file \n";
		print "argv[2]: prediction file\n";
		print "argv[3]: result (output) file\n";
		print "argv[4]: flag. If flag equals 1, output the evaluation results per query; if flag equals 0, simply output the average results.\n";
		exit -1;
}
my $fnFeature = $ARGV[0];
my $fnPrediction = $ARGV[1];
my $fnResult = $ARGV[2];
my $flag = $ARGV[3];
if($flag != 1 && $flag != 0) {
    print "Invalid command line.\n";
    print "Usage: perl Eval.pl argv[1] argv[2] argv[3] argv[4]\n";
    print "Flag should be 0 or 1\n";
    exit -1;
}
print( $fnFeature );
print( $fnPrediction );

my %hsQueryDocLabelScore = ReadInputFiles($fnFeature, $fnPrediction);
my %hsQueryEval = EvalQuery(\%hsQueryDocLabelScore);
OuputResults($fnResult, %hsQueryEval);


sub ReadInputFiles {

    my ($fnFeature, $fnPred) = @_;
    my %hsQueryDocLabelScore;
    
    if(!open(FIN_Feature, $fnFeature)) {
	print "Invalid command line.\n";
	print "Open \$fnFeature\" failed.\n";
	exit -2;
    }

    if(!open(FIN_Pred, $fnPred)) {
	print "Invalid command line.\n";
	print "Open \"$fnPred\" failed.\n";
	exit -2;
    }

    my $lineNum = 0;
    while(defined(my $lnFea = <FIN_Feature>)) {

        $lineNum ++;
        chomp($lnFea);
        my $predScore = <FIN_Pred>;
        if (!defined($predScore)) {
            print "Error to read $fnPred at line $lineNum.\n";
            exit -2;
        }
        chomp($predScore);
	print( $predScore."\n");

	#$hsQueryDocLabelScore{$qid}{$did}{"inc"} = $inc;
	#$hsQueryDocLabelScore{$qid}{$did}{"prob"} = $prob;
        if ($lnFea =~ m/^(\d+) qid\:([^\s]+).*?\#docid = ([^\s]+)/) {
            my $label = $1;
            my $qid = $2;
            my $did = $3;
            $hsQueryDocLabelScore{$qid}{$did}{"label"} = $label;
            $hsQueryDocLabelScore{$qid}{$did}{"pred"} = $predScore;
            $hsQueryDocLabelScore{$qid}{$did}{"lineNum"} = $lineNum;
        } else {
            print "Error to parse $fnFeature at line $lineNum:\n$lnFea\n";
            exit -2;
        }
    }
    close(FIN_Feature);
    close(FIN_Pred);
    return %hsQueryDocLabelScore;
    }


}

sub PrecisionAtN {

    my ($topN, @rates) = @_;
    my @PrecN;
    my $numRelevant = 0;
    #modified by Jun Xu, 2009-4-24.
    #if # retrieved doc <  $topN, the P@N will consider the hole as irrelevant
    #for(my $iPos = 0;  $iPos < $topN && $iPos < $#rates + 1; $iPos ++)    
    for (my $iPos = 0; $iPos < $topN; $iPos ++)
    {
        my $r;
        if ($iPos < $#rates + 1)
        {
            $r = $rates[$iPos];
        }
        else
        {
            $r = 0;
        }
        $numRelevant ++ if ($hsPrecisionRel{$r} == 1);
        $PrecN[$iPos] = $numRelevant / ($iPos + 1);
    }
    return @PrecN;
}


sub EvalQuery {

    my $pHash = $_[0];
    my %hsResults;
    
    my @qids = sort{$a <=> $b} keys(%$pHash);
    for(my $i = 0; $i < @qids; $i ++)
    {
        my $qid = $qids[$i];
        my @tmpDid = sort{$$pHash{$qid}{$a}{"lineNum"} <=> $$pHash{$qid}{$b}{"lineNum"}} keys(%{$$pHash{$qid}});
        my @docids = sort{$$pHash{$qid}{$b}{"pred"} <=> $$pHash{$qid}{$a}{"pred"}} @tmpDid;
        my @rates;

        for(my $iPos = 0; $iPos < $#docids + 1; $iPos ++) {
            $rates[$iPos] = $$pHash{$qid}{$docids[$iPos]}{"label"};
        }

        my $map  = MAP(@rates);
        my @PAtN = PrecisionAtN($iMaxPosition, @rates);
	#modified by Jun Xu, calculate all possible positions' NDCG for MeanNDCG
        #my @Ndcg = NDCG($iMaxPosition, @rates);
        @{$hsResults{$qid}{"PatN"}} = @PAtN;
        $hsResults{$qid}{"MAP"} = $map;
    }
    return %hsResults;
}
