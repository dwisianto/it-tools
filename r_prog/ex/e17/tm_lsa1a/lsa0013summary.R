
rm(list=ls())

source('./lsa0010const.R')



load(file=rdat.val)
d.mat <- d.raw
d.gene.raw <- t(d.raw)
d.gene.name <- d.gene.raw[1,]
d.gene.val <- d.gene.raw[-1,]
d.gene <- as.data.frame(d.gene.raw[-1,])
names(d.gene) <- d.gene.name
d.gene.dim <- dim(d.gene)



idx <- sapply(d.gene, is.factor)
d.gene[idx] <- lapply(d.gene[idx], function(x) as.numeric(as.character(x)))
for ( aCol in names(d.gene)) {
    if(is.factor(d.gene[,aCol])) {
        print(is.factor(d.gene[,aCol]))
    }
}


save(d.gene,file=rdat.gene)




## d.size.cell=ncol(d.mat)
## d.size.gene=nrow(d.mat)-1
## print(c(d.size.cell,d.size.gene))

## capture.output(print(summary(d.mat)), file=nfo.cell)
## capture.output(print(names(d.mat)), file=nfo.cell.name)

## capture.output(print(d.gene.name), file=nfo.gene.name)
capture.output(print(summary(d.gene[,1:5000])), file=nfo.gen1)
capture.output(print(summary(d.gene[,5001:10000])), file=nfo.gen2)
capture.output(print(summary(d.gene[,10001:15000])), file=nfo.gen3)
capture.output(print(summary(d.gene[,15001:20000])), file=nfo.gen4)
capture.output(print(summary(d.gene[,20001:d.gene.dim[2]])), file=nfo.gen5)

