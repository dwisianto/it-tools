letor_extract = function(filepath) {
  
  ##
  aFile = file(filepath, "r")
  allLines = readLines(aFile)
  #print(allLines[2])
  #print(allLines[5])
  close(aFile)
  
  ##
  lstMAP = strsplit(allLines[2],'\\t')[[1]]
  lstNDCG = strsplit(allLines[5],'\\t')[[1]]
  #print(lstMap)
  #print(lstNDCG)
  #print(class(lstMap))
  #print(class(lstNDCG))
  #print(lstNDCG[1])
  #print(lstNDCG[[1]])
  lstNDCG = lstNDCG[-1]
  lstMAP = lstMAP[-1]
  #print(lstMAP)
  #print(lstNDCG)
  #print(lstMAP[1])
  #print(lstNDCG[1])

  #  while ( TRUE ) {
  #  line = readLines(aFile, n = 1)
  #  if ( length(line) == 0 ) {
  #    break
  #  }
  #  print(line)
  #}
  
  return (list("map"=lstMAP,"ndcg"=lstNDCG))
}