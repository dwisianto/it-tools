
## []
## library
## source("./t0a.R")
## source("./t0cfg.R")
## source("./t3stp.R")
## source("./t3fun.R")

source("./k0aLibAttach.R")
source("./k2cfg.R")
source("./k3fun.R")

aCfg=cF5a
aCfg$stgId=1
dbLst <- stpX.db(aCfg,stgId=1)

## stpX.gbm1(aCfg, dbLst)


##
##print(dim(dat.trn))
##print(names(dat.trn))
##print(summary(dat.trn))
dat.trn = dbLst$dat.trn
dat.tst = dbLst$dat.tst
dat.val = dbLst$dat.val

## []
## print(names(dat.trn))
## print(is.factor(dat.tst$y))
##
dat.trn <- stpX.utl.cln.field.gbm(dat.trn)
dat.tst <- stpX.utl.cln.field.gbm(dat.tst)
dat.val <- stpX.utl.cln.field.gbm(dat.val)

dat.trn <- stpX.utl.binarize(dat.trn)
dat.tst <- stpX.utl.binarize(dat.tst)
dat.val <- stpX.utl.binarize(dat.val)

dat2 <- filterByVariation(dat.trn,dat.tst,dat.val)
dat.trn <- rbind(dat2$r,dat2$v)
dat.tst <- dat2$s
dat.val <- dat2$v


dat.trn$qid <- as.numeric(dat.trn$qid)

fm.n <- names(dat.trn)
fm   <- as.formula(paste('y ~ ',paste(collapse='+',fm.n[grep('x.',fm.n)])))


##distribution='gaussian', # loss function: gaussian
##            weights=w,
##            var.monotone=c(0,0,0,0,0,0), # -1: monotone decrease, +1: monotone increase, 0: no monotone restrictions
gbm1 <- dgbm.train( fm,                            # formula
		data=dat.trn,                   # dataset
		distribution="gaussian",     # bernoulli, adaboost, gaussian, poisson, and coxph available
		n.trees=10,                 # number of trees
		shrinkage=0.005,             # shrinkage or learning rate, 0.001 to 0.1 usually work
		interaction.depth=2,         # 1: additive model, 2: two-way interactions, etc
		bag.fraction = 0.5,          # subsampling fraction, 0.5 is probably best
		train.fraction = 0.7,        # fraction of data for training, first train.fraction*N used for training
		n.minobsinnode = 10,         # minimum number of obs needed in each node
		keep.data=T)
gbm1.itr <- gbm.perf(gbm1,  plot.it=FALSE, overlay=FALSE)

prdTrn=dgbm.predict(gbm1, dat.trn, gbm1.itr)
prdTst=dgbm.predict(gbm1, dat.tst, gbm1.itr)
prdVal=dgbm.predict(gbm1, dat.val, gbm1.itr)

#prdTrn=dgbm.predict.inline(gbm1, dat.trn, gbm1.itr)
#prdTst=dgbm.predict.inline(gbm1, dat.tst, gbm1.itr)
#prdVal=dgbm.predict.inline(gbm1, dat.val, gbm1.itr)

##     # yf=list(y=dat.trn$y,f=prdTrn)
##     # ir.measure.conc(y.f=yf, max.rank=5)
##     # ir.measure.auc(y.f=yf, max.rank=5)
##     # ir.measure.mrr(y.f=yf, max.rank=5)
##     # ir.measure.map(y.f=yf, max.rank=5)
##     # ir.measure.ndcg(y.f=yf, max.rank=5)



##
cfg<- aCfg
f0=file.path(cfg$gbLoc,cfg$dbLst[12],cfg$foldLstBrief[cfg$dbFoldId],cfg$gbLst[cfg$gbId],cfg$stgLstI[cfg$stgId])
fom.gold.trn=normalizePath(mustWork=FALSE, file.path(f0,cfg$foldLstTxt[1]) )
fom.gold.tst=normalizePath(mustWork=FALSE, file.path(f0,cfg$foldLstTxt[2]) )
fom.gold.val=normalizePath(mustWork=FALSE, file.path(f0,cfg$foldLstTxt[3]) )

fomTrn <-  letor_eval( exp(prdTrn) ,fom.gold.trn)
fomTst <-  letor_eval( exp(prdTst) ,fom.gold.tst)
fomVal <-  letor_eval( exp(prdVal) ,fom.gold.val)


print(rbind(fomTrn$map,fomTst$map,fomVal$map))
print(rbind(fomTrn$ndcg,fomTst$ndcg,fomVal$ndcg))

