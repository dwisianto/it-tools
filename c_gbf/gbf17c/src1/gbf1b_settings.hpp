/*
 * gbf_settings.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef GBF1B_SETTINGS_HPP_
#define GBF1B_SETTINGS_HPP_



//using namespace config4cpp;
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

namespace pt = boost::property_tree;


/**
 * http://www.boost.org/doc/libs/1_64_0/doc/html/property_tree/tutorial.html
 * std::set<std::string> m_modules;
 */
struct gbf_settings {

    int m_crows;
    int m_ccols;

    std::string m_dbloc;
    std::string m_dbname;

    std::string m_log_loc;
    std::string m_result_loc;
    std::string m_result_tree;
    std::string m_result_forest;
    std::string m_result_eval;
    std::string m_result_pred;

    std::string m_resultTreePred;
	std::string m_resultTreeEval;
	std::string m_resultTreeDot;

	std::string m_resultForestPred;
	std::string m_resultForestEval;


	int    m_iMdlForestSize;
	double m_dMdlForestShrinkage;
	double m_dMdlForestLambda;
	int    m_iMdlTreeDepth;
	int    m_iMdlTreeMinObs;



    // Use get_child to find the node containing the modules, and iterate over
    // its children. If the path cannot be resolved, get_child throws.
    // A C++11 for-range loop would also work.
    //BOOST_FOREACH(pt::ptree::value_type &v, tree.get_child("debug.modules")) {
        // The data function is used to access the data stored in a node.
        //m_modules.insert(v.second.data());
    //}
    void load(const std::string &filename) {

        // Create empty property tree object
        pt::ptree tree;

        // Parse the XML into the property tree.
        pt::read_xml(filename, tree);

        //
        m_crows = tree.get("gbf.lenrow", 0);
        m_ccols = tree.get("gbf.lencol", 0);

        //
        m_dbname = tree.get<std::string>("gbf.dbname");
        m_dbloc = tree.get<std::string>("gbf.dbloc");

        //
        m_log_loc       = tree.get<std::string>("gbf.logLoc");
        m_result_loc    = tree.get<std::string>("gbf.resultLoc");
        m_result_tree   = tree.get<std::string>("gbf.resultTree");
        m_result_forest = tree.get<std::string>("gbf.resultForest");
        m_result_eval   = tree.get<std::string>("gbf.resultEval");
        m_result_pred   = tree.get<std::string>("gbf.resultPred");

        m_resultTreePred   = tree.get<std::string>("gbf.resultTreePred");
    	m_resultTreeEval   = tree.get<std::string>("gbf.resultTreeEval");
    	m_resultTreeDot    = tree.get<std::string>("gbf.resultTreeDot");

    	m_resultForestPred = tree.get<std::string>("gbf.resultForestPred");
    	m_resultForestEval = tree.get<std::string>("gbf.resultForestEval");

    	m_iMdlForestSize = tree.get("gbf.modelForestSize",2);
    	m_dMdlForestShrinkage = tree.get("gbf.modelForestShrinkage",0.5);
    	m_dMdlForestLambda = tree.get("gbf.modelForestLambda",0.1);

    	m_iMdlTreeDepth  = tree.get("gbf.modelTreeDepth",4);
    	m_iMdlTreeMinObs = tree.get("gbf.modelTreeMinObs",2);

    }


    /**
     *
     */
    void save(const std::string &filename) {

        // Create an empty property tree object.
        pt::ptree tree;

        // Put the simple values into the tree.
        // The integer is automatically converted to a string.
        // Note that the "debug" node is automatically created if it doesn't exist.
        //tree.put("debug.filename", m_crows);
        //tree.put("debug.level", m_ccols);
    	tree.put("gbf.lenrow",m_crows);
    	tree.put("gbf.lencol",m_ccols);

    	tree.put("gbf.dbname",m_dbname);
    	tree.put("gbf.dbloc",m_dbloc);

    	tree.put("gbf.logLoc",m_log_loc);
    	tree.put("gbf.resultLoc",m_result_loc);
    	tree.put("gbf.resultTree",m_result_tree);
    	tree.put("gbf.resultForest",m_result_forest );
    	tree.put("gbf.resultEval",m_result_eval);
    	tree.put("gbf.resultPred",m_result_pred);

    	tree.put("gbf.resultTreePred",m_resultTreePred );
    	tree.put("gbf.resultTreeEval",m_resultTreeEval );
    	tree.put("gbf.resultTreeDot",m_resultTreeDot );
    	tree.put("gbf.resultForestPred",m_resultForestPred );
    	tree.put("gbf.resultForestEval",m_resultForestEval);

    	tree.put("gbf.modelForestSize",m_iMdlForestSize );
        	tree.put("gbf.modelForestShrinkage",m_dMdlForestShrinkage);
        	tree.put("gbf.modelForestLambda",m_dMdlForestLambda);

        	tree.put("gbf.modelTreeDepth",m_iMdlTreeDepth );
        	tree.put("gbf.modelTreeMinObs",m_iMdlTreeMinObs );


        // Add all the modules.
        // Unlike put, which overwrites existing nodes,
        // add adds a new node at the lowest level,
        // so the "modules" node will have multiple "module" children.
        //BOOST_FOREACH(const std::string &name, m_modules)
        //tree.add("debug.modules.module", name);

        // Write property tree to XML file
        pt::write_xml(filename, tree);
    }

    /**
     *
     */
    std::string toString() {

    	std::string sOut = "";

    	sOut += " m_dbloc  "+m_dbloc+"\n";
    	sOut += " m_dbname "+m_dbname+"\n";
    	sOut += " m_crows  "+std::to_string(m_crows)+"\n";
    	sOut += " m_ccols  "+std::to_string(m_ccols)+"\n";

    	return sOut;
    }

};




#endif /* GBF1B_SETTINGS_HPP_ */
