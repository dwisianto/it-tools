/*
 * art2a_dataset.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART2A_DATASET_HPP_
#define ART2A_DATASET_HPP_


//#include<cassert>

#include "art1a_build_info.hpp"
//#include "stdlib.h"

class CDataset {

	public:

    bool fHasOffset;
    double *adX;
    int *aiXOrder;
    double *adXTemp4Order;

    double *adY;
    double *adOffset;
    double *adWeight;
    double *adMisc;
    char **apszVarNames;
    int *acVarClasses;
    int *alMonotoneVar;

    int cRows;
    int cCols;

	CDataset() {

	    fHasOffset = false;
	    adX = NULL;
	    aiXOrder = NULL;
	    adXTemp4Order = NULL;
	    adY = NULL;
	    adOffset = NULL;
	    adWeight = NULL;
	    apszVarNames = NULL;

	    cRows = 0;
	    cCols = 0;
	}

	~CDataset();

    HRESULT ResetWeights() {

        HRESULT hr = S_OK;
        int i = 0;

        if(adWeight == NULL) {
            hr = E_INVALIDARG;
            ErrorTrace(hr);
            goto Error;
        }

        for(i=0; i<cRows; i++) {
            adWeight[i] = 1.0;
        }

    Cleanup:
        return hr;
    Error:
        goto Cleanup;
    }


    HRESULT SetData
    (
        double *adX,
        int *aiXOrder,
        double *adY,
        double *adOffset,
        double *adWeight,
        double *adMisc,
        int cRows,
        int cCols,
        int *acVarClasses,
        int *alMonotoneVar
    )
    {
        HRESULT hr = S_OK;

        if((adX == NULL) || (adY == NULL))
        {
            hr = E_INVALIDARG;
            ErrorTrace(hr);
            goto Error;
        }

        this->cRows = cRows;
        this->cCols = cCols;

        this->adX = adX;
        this->aiXOrder = aiXOrder;
        this->adY = adY;
        this->adOffset = adOffset;
        this->adWeight = adWeight;
        this->acVarClasses = acVarClasses;
        this->alMonotoneVar = alMonotoneVar;

        if((adOffset != NULL) && !ISNAN(*adOffset))
        {
            this->adOffset = adOffset;
            fHasOffset = true;
        }
        else
        {
            this->adOffset = NULL;
            fHasOffset = false;
        }
        if((adMisc != NULL) && !ISNAN(*adMisc))
        {
            this->adMisc = adMisc;
        }
        else
        {
            this->adMisc = NULL;
        }

    Cleanup:
       return hr;
    Error:
        goto Cleanup;
    }



    HRESULT Entry(int iRow,
                  int iCol,
                  double &dValue)
    {
        HRESULT hr = S_OK;

        if((iRow >= cRows) || (iCol >= cCols))
        {
            hr = E_INVALIDARG;
            ErrorTrace(hr);
            goto Error;
        }

        dValue = adX[iCol*cRows + iRow];

    Cleanup:
        return hr;
    Error:
        goto Cleanup;
    }


};













#endif /* ART2A_DATASET_HPP_ */
