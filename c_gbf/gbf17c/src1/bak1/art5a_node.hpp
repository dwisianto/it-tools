
#ifndef ART5A_NODE_HPP_
#define ART5A_NODE_HPP_


#include <vector>
#include "art1a_build_info.hpp"
#include "art2a_dataset.hpp"

//#include "art5b_node_factory.hpp"
//class CNodeFactory;

using namespace std;

typedef vector<char> VEC_CATEGORIES;
typedef vector<VEC_CATEGORIES> VEC_VEC_CATEGORIES;


class CNode {

	public:

    double dPrediction;
    double dTrainW;   // total training weight in node
    unsigned long cN; // number of training observations in node
    bool isTerminal;


    CNode() {
        dPrediction = 0.0;
        dTrainW = 0.0;
        isTerminal = false;
    }

    virtual ~CNode()
    {
        // the nodes get deleted by deleting the node factory
    }

    virtual HRESULT Adjust
    (
        unsigned long cMinObsInNode
    )
    {
        HRESULT hr = E_NOTIMPL;
        ErrorTrace(hr);
        return hr;
    }



    virtual HRESULT Predict(CDataset *pData,
                            unsigned long iRow,
                            double &dFadj)
    {
        HRESULT hr = E_NOTIMPL;
        ErrorTrace(hr);
        return hr;
    }

    virtual HRESULT Predict(double *adX,
                            unsigned long cRow,
                            unsigned long cCol,
                            unsigned long iRow,
                            double &dFadj) = 0;
    static double Improvement
    (
        double dLeftW,
        double dRightW,
        double dMissingW,
        double dLeftSum,
        double dRightSum,
        double dMissingSum
    )
    {
        double dTemp = 0.0;
        double dResult = 0.0;

        if(dMissingW == 0.0)
        {
            dTemp = dLeftSum/dLeftW - dRightSum/dRightW;
            dResult = dLeftW*dRightW*dTemp*dTemp/(dLeftW+dRightW);
        }
        else
        {
            dTemp = dLeftSum/dLeftW - dRightSum/dRightW;
            dResult += dLeftW*dRightW*dTemp*dTemp;
            dTemp = dLeftSum/dLeftW - dMissingSum/dMissingW;
            dResult += dLeftW*dMissingW*dTemp*dTemp;
            dTemp = dRightSum/dRightW - dMissingSum/dMissingW;
            dResult += dRightW*dMissingW*dTemp*dTemp;
            dResult /= (dLeftW + dRightW + dMissingW);
        }

        return dResult;
    }


    //virtual HRESULT PrintSubtree(unsigned long cIndent);
    virtual HRESULT PrintSubtree
    (
        unsigned long cIndent
    )
    {
        HRESULT hr = E_NOTIMPL;
        ErrorTrace(hr);
        return hr;
    }



    virtual HRESULT TransferTreeToRList(int &iNodeID,
                                        CDataset *pData,
                                        int *aiSplitVar,
                                        double *adSplitPoint,
                                        int *aiLeftNode,
                                        int *aiRightNode,
                                        int *aiMissingNode,
                                        double *adErrorReduction,
                                        double *adWeight,
                                        VEC_VEC_CATEGORIES &vecSplitCodes,
                                        int cCatSplitsOld,
                                        double dShrinkage)
    {
        return E_NOTIMPL;
    }


    double TotalError()
    {
        HRESULT hr = E_NOTIMPL;
        ErrorTrace(hr);
        return hr;
    }


    virtual HRESULT GetVarRelativeInfluence
    (
        double *adRelInf
    )
    {
        HRESULT hr = E_NOTIMPL;
        ErrorTrace(hr);
        return hr;
    }

    virtual HRESULT RecycleSelf(CNodeFactory *pNodeFactory) = 0;











protected:
    double GetXEntry(CDataset *pData,
                            unsigned long iRow,
                            unsigned long iCol) {
        return pData->adX[iCol*(pData->cRows) + iRow];
    }

};

typedef CNode *PCNode;





#endif
