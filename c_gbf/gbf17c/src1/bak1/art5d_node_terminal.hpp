/*
 * art5d_node_terminal.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART5D_NODE_TERMINAL_HPP_
#define ART5D_NODE_TERMINAL_HPP_

#include <vector>
#include "gbf1c_logging.hpp"
#include "art2a_dataset.hpp"
#include "art5a_node.hpp"
#include "art5d_node_terminal.hpp"
#include "art5b_node_factory.hpp"


using namespace std;

class CNodeTerminal : public CNode
{
public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("NodeTerminal"));

	CNodeTerminal() {
	    isTerminal = true;
	}


	~CNodeTerminal() {
	    mLog.info("terminal destructor");
	}


    HRESULT Adjust(unsigned long cMinObsInNode) {
        return S_OK;
    }


    HRESULT PrintSubtree
    (
        unsigned long cIndent
    )
    {
        unsigned long i = 0;

        string sIndent="";
        for(i=0; i< cIndent; i++) {
        	sIndent +="  ";
        	printf("  ");
        }
        printf("N=%f, Prediction=%lf",dTrainW,dPrediction);
        mLog.info("%s, N=%f, Prediction=%lf ",sIndent.c_str(),dTrainW,dPrediction);

        return S_OK;
    }



    HRESULT TransferTreeToRList
    (
        int &iNodeID,
        CDataset *pData,
        int *aiSplitVar,
        double *adSplitPoint,
        int *aiLeftNode,
        int *aiRightNode,
        int *aiMissingNode,
        double *adErrorReduction,
        double *adWeight,
        VEC_VEC_CATEGORIES &vecSplitCodes,
        int cCatSplitsOld,
        double dShrinkage
    )
    {
        HRESULT hr = S_OK;

        aiSplitVar[iNodeID] = -1;
        adSplitPoint[iNodeID] = dShrinkage*dPrediction;
        aiLeftNode[iNodeID] = -1;
        aiRightNode[iNodeID] = -1;
        aiMissingNode[iNodeID] = -1;
        adErrorReduction[iNodeID] = 0.0;
        adWeight[iNodeID] = dTrainW;

        iNodeID++;

        return hr;
    }



    HRESULT ApplyShrinkage(double dLambda)
    {
        HRESULT hr = S_OK;
        dPrediction *= dLambda;

        return hr;
    }

    HRESULT Predict
    (
        CDataset *pData,
        unsigned long iRow,
        double &dFadj
    )
    {
        dFadj = dPrediction;

        return S_OK;
    }



    HRESULT Predict
    (
        double *adX,
        unsigned long cRow,
        unsigned long cCol,
        unsigned long iRow,
        double &dFadj
    )
    {
        dFadj = dPrediction;

        return S_OK;
    }


    HRESULT GetVarRelativeInfluence
    (
        double *adRelInf
    )
    {
        return S_OK;
    }

    HRESULT RecycleSelf
    (
        CNodeFactory *pNodeFactory
    )
    {
        pNodeFactory->RecycleNode(this);
        return S_OK;
    };

};



typedef CNodeTerminal *PCNodeTerminal;
typedef vector<PCNodeTerminal> VEC_P_NODETERMINAL;

#endif /* ART5D_NODE_TERMINAL_HPP_ */

















