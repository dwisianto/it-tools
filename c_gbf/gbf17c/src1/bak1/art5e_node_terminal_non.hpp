/*
 * art5e_node_terminal_non.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART5E_NODE_TERMINAL_NON_HPP_
#define ART5E_NODE_TERMINAL_NON_HPP_

#include "art5a_node.hpp"
#include "art5b_node_factory.hpp"
#include "art5d_node_terminal.hpp"

class CNodeNonterminal : public CNode
{
public:

    CNode *pLeftNode;
    CNode *pRightNode;
    CNode *pMissingNode;
    unsigned long iSplitVar;
    double dImprovement;


    CNodeNonterminal()
    {
        pLeftNode = NULL;
        pRightNode = NULL;
        iSplitVar = 0;
        dImprovement = 0.0;
        pMissingNode = NULL;
    }


    virtual ~CNodeNonterminal()
    {

    }


    virtual HRESULT Adjust
    (
        unsigned long cMinObsInNode
    )
    {
        HRESULT hr = S_OK;

        hr = pLeftNode->Adjust(cMinObsInNode);
        hr = pRightNode->Adjust(cMinObsInNode);

        if(pMissingNode->isTerminal && (pMissingNode->cN < cMinObsInNode))
        {
            dPrediction = ((pLeftNode->dTrainW)*(pLeftNode->dPrediction) +
                           (pRightNode->dTrainW)*(pRightNode->dPrediction))/
                          (pLeftNode->dTrainW + pRightNode->dTrainW);
            pMissingNode->dPrediction = dPrediction;
        }
        else
        {
            hr = pMissingNode->Adjust(cMinObsInNode);
            dPrediction =
                ((pLeftNode->dTrainW)*   (pLeftNode->dPrediction) +
                 (pRightNode->dTrainW)*  (pRightNode->dPrediction) +
                 (pMissingNode->dTrainW)*(pMissingNode->dPrediction))/
                (pLeftNode->dTrainW + pRightNode->dTrainW + pMissingNode->dTrainW);
        }

        return hr;
    }


    virtual signed char WhichNode(CDataset *pData,
                                  unsigned long iObs) = 0;
    virtual signed char WhichNode(double *adX,
                                  unsigned long cRow,
                                  unsigned long cCol,
                                  unsigned long iRow) = 0;
    virtual HRESULT TransferTreeToRList(int &iNodeID,
                                        CDataset *pData,
                                        int *aiSplitVar,
                                        double *adSplitPoint,
                                        int *aiLeftNode,
                                        int *aiRightNode,
                                        int *aiMissingNode,
                                        double *adErrorReduction,
                                        double *adWeight,
                                        VEC_VEC_CATEGORIES &vecSplitCodes,
                                        int cCatSplitsOld,
                                        double dShrinkage) = 0;


    HRESULT Predict
    (
        CDataset *pData,
        unsigned long iRow,
        double &dFadj
    )
    {
        HRESULT hr = S_OK;

        signed char schWhichNode = WhichNode(pData,iRow);
        if(schWhichNode == -1)
        {
            hr = pLeftNode->Predict(pData, iRow, dFadj);
        }
        else if(schWhichNode == 1)
        {
            hr = pRightNode->Predict(pData, iRow, dFadj);
        }
        else
        {
            hr = pMissingNode->Predict(pData, iRow, dFadj);
        }

        return hr;
    }


    HRESULT Predict
    (
        double *adX,
        unsigned long cRow,
        unsigned long cCol,
        unsigned long iRow,
        double &dFadj
    )
    {
        HRESULT hr = S_OK;

        signed char schWhichNode = WhichNode(adX,cRow,cCol,iRow);
        if(schWhichNode == -1)
        {
            hr = pLeftNode->Predict(adX,cRow,cCol,iRow,dFadj);
        }
        else if(schWhichNode == 1)
        {
            hr = pRightNode->Predict(adX,cRow,cCol,iRow,dFadj);
        }
        else
        {
            hr = pMissingNode->Predict(adX,cRow,cCol,iRow,dFadj);
        }

        return hr;
    }




    HRESULT GetVarRelativeInfluence
    (
        double *adRelInf
    )
    {
        HRESULT hr = S_OK;

        adRelInf[iSplitVar] += dImprovement;
        pLeftNode->GetVarRelativeInfluence(adRelInf);
        pRightNode->GetVarRelativeInfluence(adRelInf);


        return hr;
    }


    virtual HRESULT RecycleSelf(CNodeFactory *pNodeFactory) = 0;

};

typedef CNodeNonterminal *PCNodeNonterminal;

#endif /* ART5E_NODE_TERMINAL_NON_HPP_ */
