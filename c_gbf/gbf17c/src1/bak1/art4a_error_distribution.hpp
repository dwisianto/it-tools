/*
 * art4a_distribution.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART4A_ERROR_DISTRIBUTION_HPP_
#define ART4A_ERROR_DISTRIBUTION_HPP_

#include "art5d_node_terminal.hpp"

class CDistribution {

	public:
    CDistribution();
    virtual ~CDistribution();

    virtual HRESULT ComputeWorkingResponse(double *adY,
                                           double *adMisc,
                                           double *adOffset,
                                           double *adF,
                                           double *adZ,
                                           double *adWeight,
                                           bool *afInBag,
                                           unsigned long nTrain) = 0;

    virtual HRESULT InitF(double *adY,
                          double *adMisc,
                          double *adOffset,
                          double *adWeight,
                          double &dInitF,
                          unsigned long cLength) = 0;

    virtual double LogLikelihood(double *adY,
                                 double *adMisc,
                                 double *adOffset,
                                 double *adWeight,
                                 double *adF,
                                 unsigned long cLength) = 0;

    virtual HRESULT FitBestConstant(double *adY,
                                    double *adMisc,
                                    double *adOffset,
                                    double *adW,
                                    double *adF,
                                    double *adZ,
                                    unsigned long *aiNodeAssign,
                                    unsigned long nTrain,
                                    VEC_P_NODETERMINAL vecpTermNodes,
                                    unsigned long cTermNodes,
                                    unsigned long cMinObsInNode,
                                    bool *afInBag,
                                    double *adFadj) = 0;


    virtual double BagImprovement(double *adY,
                                  double *adMisc,
                                  double *adOffset,
                                  double *adWeight,
                                  double *adF,
                                  double *adFadj,
                                  bool *afInBag,
                                  double dStepSize,
                                  unsigned long nTrain) = 0;

};

typedef CDistribution *PCDistribution;

#endif /* ART4A_ERROR_DISTRIBUTION_HPP_ */
