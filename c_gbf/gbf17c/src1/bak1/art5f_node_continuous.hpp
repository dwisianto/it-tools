/*
 * art5f_node_continuous.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART5F_NODE_CONTINUOUS_HPP_
#define ART5F_NODE_CONTINUOUS_HPP_

//#include "art5node_continuous.hpp"
#include "art5b_node_factory.hpp"


#include <float.h>
#include "art5e_node_terminal_non.hpp"

class CNodeContinuous : public CNodeNonterminal
{
public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("NodeCont"));


    double dSplitValue;


	CNodeContinuous()
	{
	    dSplitValue = 0.0;
	}


	~CNodeContinuous()
	{
	}


    HRESULT PrintSubtree
    (
        unsigned long cIndent
    )
    {
        HRESULT hr = S_OK;
        unsigned long i = 0;

        std::string sIndent="";    for(i=0; i< cIndent; i++) {    	sIndent +="  ";    }
        printf("%s N=%f, Pred=%f, Impr=%f\n", //, NA pred=%lf\n",
        		sIndent.c_str(),
                dTrainW,
                dImprovement,
                dPrediction);
                //(pMissingNode == NULL ? 0.0 : pMissingNode->dPrediction));
        mLog.info("%s N=%f, Pred=%f, Impr=%f",
        		sIndent.c_str(),
                dTrainW,
                dImprovement,
                dPrediction);

        sIndent=""; for(i=0; i< cIndent; i++) {    	sIndent += "  ";    }
        printf("%s L%lu V%lu < %lf\n",sIndent.c_str(), cIndent, iSplitVar,dSplitValue);
        mLog.info("%s L%lu V%lu < %lf",sIndent.c_str(),cIndent, iSplitVar,dSplitValue);
        if(pLeftNode != NULL) {      hr = pLeftNode->PrintSubtree(cIndent+1);    }

        sIndent=""; for(i=0; i< cIndent; i++) {    	sIndent += "  ";    }
        printf("\n%s R%lu V%lu > %lf\n",sIndent.c_str(), cIndent, iSplitVar,dSplitValue);
        mLog.info("%s R%lu V%lu > %lf",sIndent.c_str(),cIndent, iSplitVar,dSplitValue);
        if(pRightNode != NULL) {      hr = pRightNode->PrintSubtree(cIndent+1);    }

        return hr;
    }

    signed char WhichNode
    (
        CDataset *pData,
        unsigned long iObs
    )
    {
        signed char ReturnValue = 0;
        double dX = pData->adX[iSplitVar*(pData->cRows) + iObs];

        if(!ISNAN(dX))
        {
            if(dX < dSplitValue)
            {
                ReturnValue = -1;
            }
            else
            {
                ReturnValue = 1;
            }
        }
        // if missing value returns 0
        mLog.info(" WhichNode RtrnVal %d dSpltVal %f iObs %lu iSplitVar %lu cRows %d",ReturnValue, dSplitValue,iObs, iSplitVar, pData->cRows);

        return ReturnValue;
    }


    signed char WhichNode
    (
        double *adX,
        unsigned long cRow,
        unsigned long cCol,
        unsigned long iRow
    )
    {
        signed char ReturnValue = 0;
        double dX = adX[iSplitVar*cRow + iRow];

        if(!ISNAN(dX))
        {
            if(dX < dSplitValue)
            {
                ReturnValue = -1;
            }
            else
            {
                ReturnValue = 1;
            }
        }
        // if missing value returns 0

        mLog.info(" WhichNode:: RetVal %u dSpltVal %f cRow %lu iRow %lu cCol %lu",ReturnValue, dSplitValue, cRow,iRow, cCol);
        return ReturnValue;
    }



    HRESULT RecycleSelf
    (
        CNodeFactory *pNodeFactory
    )
    {
        HRESULT hr = S_OK;
        pNodeFactory->RecycleNode(this);
        return hr;
    }



    HRESULT TransferTreeToRList
    (
        int &iNodeID,
        CDataset *pData,
        int *aiSplitVar,
        double *adSplitPoint,
        int *aiLeftNode,
        int *aiRightNode,
        int *aiMissingNode,
        double *adErrorReduction,
        double *adWeight,
        VEC_VEC_CATEGORIES &vecSplitCodes,
        int cCatSplitsOld,
        double dShrinkage
    )
    {
        HRESULT hr = S_OK;
        int iThisNodeID = iNodeID;

        aiSplitVar[iThisNodeID] = iSplitVar;
        adSplitPoint[iThisNodeID] = dSplitValue;
        adErrorReduction[iThisNodeID] = dImprovement;
        adWeight[iThisNodeID] = dTrainW;


        iNodeID++;
        aiLeftNode[iThisNodeID] = iNodeID;
        hr = pLeftNode->TransferTreeToRList(iNodeID,
                                            pData,
                                            aiSplitVar,
                                            adSplitPoint,
                                            aiLeftNode,
                                            aiRightNode,
                                            aiMissingNode,
                                            adErrorReduction,
                                            adWeight,
                                            vecSplitCodes,
                                            cCatSplitsOld,
                                            dShrinkage);
        if(FAILED(hr)) goto Error;

        aiRightNode[iThisNodeID] = iNodeID;
        hr = pRightNode->TransferTreeToRList(iNodeID,
                                             pData,
                                             aiSplitVar,
                                             adSplitPoint,
                                             aiLeftNode,
                                             aiRightNode,
                                             aiMissingNode,
                                             adErrorReduction,
                                             adWeight,
                                             vecSplitCodes,
                                             cCatSplitsOld,
                                             dShrinkage);
        if(FAILED(hr)) goto Error;

        aiMissingNode[iThisNodeID] = iNodeID;
        hr = pMissingNode->TransferTreeToRList(iNodeID,
                                               pData,
                                               aiSplitVar,
                                               adSplitPoint,
                                               aiLeftNode,
                                               aiRightNode,
                                               aiMissingNode,
                                               adErrorReduction,
                                               adWeight,
                                               vecSplitCodes,
                                               cCatSplitsOld,
                                               dShrinkage);
        if(FAILED(hr)) goto Error;

    Cleanup:
        return hr;
    Error:
        goto Cleanup;
    }

};

typedef CNodeContinuous *PCNodeContinuous;


#endif /* ART5F_NODE_CONTINUOUS_HPP_ */
