/*
 * art5c_node_search.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART5C_NODE_SEARCH_HPP_
#define ART5C_NODE_SEARCH_HPP_


#include "dataset.h"
#include "node_factory.h"

using namespace std;

class CNodeSearch
{
public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("NodeSrch"));

	CNodeSearch()
	    :k_cMaxClasses(256)
	{
		mLog.info("Constructor");
	    iBestSplitVar = 0;
	    dBestSplitValue = 0.0;
	    fIsSplit = false;

	    dBestMissingTotalW = 0.0;
	    dCurrentMissingTotalW = 0.0;
	    dBestMissingSumZ = 0.0;
	    dCurrentMissingSumZ = 0.0;

	    adGroupSumZ = NULL;
	    adGroupW = NULL;
	    acGroupN = NULL;
	    adGroupMean = NULL;
	    aiCurrentCategory = NULL;
	    aiBestCategory = NULL;

	    iRank = UINT_MAX;
	}


	~CNodeSearch()
	{
		mLog.info("Destructor");
	    if(adGroupSumZ != NULL)
	    {
	        delete [] adGroupSumZ; delete_item(adGroupSumZ);
	        adGroupSumZ = NULL;
	    }
	    if(adGroupW != NULL)
	    {
	        delete [] adGroupW; delete_item(adGroupW);
	        adGroupW = NULL;
	    }
	    if(acGroupN != NULL)
	    {
	        delete [] acGroupN; delete_item(acGroupN);
	        acGroupN = NULL;
	    }
	    if(adGroupMean != NULL)
	    {
	        delete [] adGroupMean; delete_item(adGroupMean);
	        adGroupMean = NULL;
	    }
	    if(aiCurrentCategory != NULL)
	    {
	        delete [] aiCurrentCategory; delete_item(aiCurrentCategory);
	        aiCurrentCategory = NULL;
	    }
	    if(aiBestCategory != NULL)
	    {
	        delete [] aiBestCategory; delete_item(aiBestCategory);
	        aiBestCategory = NULL;
	    }
	}


    HRESULT Initialize
    (
        unsigned long cMinObsInNode
    )
    {
    	mLog.info("Initialize");
        HRESULT hr = S_OK;

        adGroupSumZ = new double[k_cMaxClasses]; new_item(adGroupSumZ);
        if(adGroupSumZ == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }
        adGroupW = new double[k_cMaxClasses]; new_item(adGroupW);
        if(adGroupW == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }
        acGroupN = new ULONG[k_cMaxClasses]; new_item(acGroupN);
        if(acGroupN == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }
        adGroupMean = new double[k_cMaxClasses]; new_item(adGroupMean);
        if(adGroupMean == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }
        aiCurrentCategory = new int[k_cMaxClasses]; new_item(aiCurrentCategory);
        if(aiCurrentCategory == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }
        aiBestCategory = new ULONG[k_cMaxClasses]; new_item(aiBestCategory);
        if(aiBestCategory == NULL)
        {
            hr = E_OUTOFMEMORY;
            ErrorTrace(hr);
            goto Error;
        }

        this->cMinObsInNode = cMinObsInNode;

    Cleanup:
        return hr;
    Error:
        goto Cleanup;
    }


    HRESULT IncorporateObs
    (
        double dX,
        double dZ,
        double dW,
        long lMonotone
    )
    {
        HRESULT hr = S_OK;
        static double dWZ = 0.0;

        if(fIsSplit) goto Cleanup;

        dWZ = dW*dZ;

        if(ISNAN(dX))
        {
            dCurrentMissingSumZ += dWZ;
            dCurrentMissingTotalW += dW;
            cCurrentMissingN++;
            dCurrentRightSumZ -= dWZ;
            dCurrentRightTotalW -= dW;
            cCurrentRightN--;
        }
        else if(cCurrentVarClasses == 0)   // variable is continuous
        {
            assert(dLastXValue <= dX);

            // Evaluate the current split
            // the newest observation is still in the right child
            dCurrentSplitValue = 0.5*(dLastXValue + dX);
            if((dLastXValue != dX) &&
               (cCurrentLeftN >= cMinObsInNode) &&
               (cCurrentRightN >= cMinObsInNode) &&
               ((lMonotone==0) ||
                (lMonotone*(dCurrentRightSumZ*dCurrentLeftTotalW -
                            dCurrentLeftSumZ*dCurrentRightTotalW) > 0)))
            {
                dCurrentImprovement =
                    CNode::Improvement(dCurrentLeftTotalW,dCurrentRightTotalW,
                                       dCurrentMissingTotalW,
                                       dCurrentLeftSumZ,dCurrentRightSumZ,
                                       dCurrentMissingSumZ);
                if(dCurrentImprovement > dBestImprovement)
                {
                    iBestSplitVar = iCurrentSplitVar;
                    dBestSplitValue = dCurrentSplitValue;
                    cBestVarClasses = 0;

                    dBestLeftSumZ    = dCurrentLeftSumZ;
                    dBestLeftTotalW  = dCurrentLeftTotalW;
                    cBestLeftN       = cCurrentLeftN;
                    dBestRightSumZ   = dCurrentRightSumZ;
                    dBestRightTotalW = dCurrentRightTotalW;
                    cBestRightN      = cCurrentRightN;
                    dBestImprovement = dCurrentImprovement;
                }
            }

            // now move the new observation to the left
            // if another observation arrives we will evaluate this
            dCurrentLeftSumZ += dWZ;
            dCurrentLeftTotalW += dW;
            cCurrentLeftN++;
            dCurrentRightSumZ -= dWZ;
            dCurrentRightTotalW -= dW;
            cCurrentRightN--;

            dLastXValue = dX;
        }
        else // variable is categorical, evaluates later
        {
            adGroupSumZ[(unsigned long)dX] += dWZ;
            adGroupW[(unsigned long)dX] += dW;
            acGroupN[(unsigned long)dX] ++;
        }

    Cleanup:
        return hr;
    }


    HRESULT Set
    (
        double dSumZ,
        double dTotalW,
        unsigned long cTotalN,
        CNodeTerminal *pThisNode,
        CNode **ppParentPointerToThisNode,
        CNodeFactory *pNodeFactory
    )
    {
    	mLog.info("Set");
        HRESULT hr = S_OK;

        dInitSumZ = dSumZ;
        dInitTotalW = dTotalW;
        cInitN = cTotalN;

        dBestLeftSumZ       = 0.0;
        dBestLeftTotalW     = 0.0;
        cBestLeftN          = 0;
        dCurrentLeftSumZ    = 0.0;
        dCurrentLeftTotalW  = 0.0;
        cCurrentLeftN       = 0;

        dBestRightSumZ      = dSumZ;
        dBestRightTotalW    = dTotalW;
        cBestRightN         = cTotalN;
        dCurrentRightSumZ   = 0.0;
        dCurrentRightTotalW = dTotalW;
        cCurrentRightN      = cTotalN;

        dBestMissingSumZ      = 0.0;
        dBestMissingTotalW    = 0.0;
        cBestMissingN         = 0;
        dCurrentMissingSumZ   = 0.0;
        dCurrentMissingTotalW = 0.0;
        cCurrentMissingN      = 0;

        dBestImprovement    = 0.0;
        iBestSplitVar       = UINT_MAX;

        dCurrentImprovement = 0.0;
        iCurrentSplitVar    = UINT_MAX;
        dCurrentSplitValue  = -HUGE_VAL;

        fIsSplit = false;

        this->pThisNode = pThisNode;
        this->ppParentPointerToThisNode = ppParentPointerToThisNode;
        this->pNodeFactory = pNodeFactory;

        return hr;
    }


    HRESULT ResetForNewVar
    (
        unsigned long iWhichVar,
        long cCurrentVarClasses
    )
    {
    	mLog.info("ResetForNewVar");
        HRESULT hr = S_OK;
        long i=0;

        if(fIsSplit) goto Cleanup;

        for(i=0; i<cCurrentVarClasses; i++)
        {
            adGroupSumZ[i] = 0.0;
            adGroupW[i] = 0.0;
            acGroupN[i] = 0;
        }

        iCurrentSplitVar = iWhichVar;
        this->cCurrentVarClasses = cCurrentVarClasses;

        dCurrentLeftSumZ      = 0.0;
        dCurrentLeftTotalW    = 0.0;
        cCurrentLeftN         = 0;
        dCurrentRightSumZ     = dInitSumZ;
        dCurrentRightTotalW   = dInitTotalW;
        cCurrentRightN        = cInitN;
        dCurrentMissingSumZ   = 0.0;
        dCurrentMissingTotalW = 0.0;
        cCurrentMissingN      = 0;

        dCurrentImprovement = 0.0;

        dLastXValue = -HUGE_VAL;

    Cleanup:
        return hr;
    }


    double BestImprovement() { return dBestImprovement; }
    HRESULT SetToSplit()
    {
        fIsSplit = true;
        return S_OK;
    };


    HRESULT CNodeSearch::SetupNewNodes
    (
        PCNodeNonterminal &pNewSplitNode,
        PCNodeTerminal &pNewLeftNode,
        PCNodeTerminal &pNewRightNode,
        PCNodeTerminal &pNewMissingNode
    )
    {
    	mLog.info("SetupNewNodes");
        HRESULT hr = S_OK;
        CNodeContinuous *pNewNodeContinuous = NULL;
        CNodeCategorical *pNewNodeCategorical = NULL;
        unsigned long i=0;

        pNewLeftNode    = pNodeFactory->GetNewNodeTerminal();
        pNewRightNode   = pNodeFactory->GetNewNodeTerminal();
        pNewMissingNode = pNodeFactory->GetNewNodeTerminal();

        // set up a continuous split
        if(cBestVarClasses==0)
        {
            pNewNodeContinuous = pNodeFactory->GetNewNodeContinuous();

            pNewNodeContinuous->dSplitValue = dBestSplitValue;
            pNewNodeContinuous->iSplitVar = iBestSplitVar;

            pNewSplitNode = pNewNodeContinuous;
        }
        else
        {
            // get a new categorical node and its branches
            pNewNodeCategorical = pNodeFactory->GetNewNodeCategorical();

            // set up the categorical split
            pNewNodeCategorical->iSplitVar = iBestSplitVar;
            pNewNodeCategorical->cLeftCategory = (ULONG)dBestSplitValue + 1;
            pNewNodeCategorical->aiLeftCategory =
                new ULONG[pNewNodeCategorical->cLeftCategory]; new_item(pNewNodeCategorical->aiLeftCategory);
            for(i=0; i<pNewNodeCategorical->cLeftCategory; i++)
            {
                pNewNodeCategorical->aiLeftCategory[i] = aiBestCategory[i];
            }

            pNewSplitNode = pNewNodeCategorical;
        }

        *ppParentPointerToThisNode = pNewSplitNode;

        pNewSplitNode->dPrediction  = pThisNode->dPrediction;
        pNewSplitNode->dImprovement = dBestImprovement;
        pNewSplitNode->dTrainW      = pThisNode->dTrainW;
        pNewSplitNode->pLeftNode    = pNewLeftNode;
        pNewSplitNode->pRightNode   = pNewRightNode;
        pNewSplitNode->pMissingNode = pNewMissingNode;

        pNewLeftNode->dPrediction    = dBestLeftSumZ/dBestLeftTotalW;
        pNewLeftNode->dTrainW        = dBestLeftTotalW;
        pNewLeftNode->cN             = cBestLeftN;
        pNewRightNode->dPrediction   = dBestRightSumZ/dBestRightTotalW;
        pNewRightNode->dTrainW       = dBestRightTotalW;
        pNewRightNode->cN            = cBestRightN;
        pNewMissingNode->dPrediction = dBestMissingSumZ/dBestMissingTotalW;
        pNewMissingNode->dTrainW     = dBestMissingTotalW;
        pNewMissingNode->cN          = cBestMissingN;

        pThisNode->RecycleSelf(pNodeFactory);

        return hr;
    }


    //HRESULT EvaluateCategoricalSplit();

    HRESULT WrapUpCurrentVariable()
    {
    	mLog.info("WrapUpCurrentVariable");
        HRESULT hr = S_OK;
        if(iCurrentSplitVar == iBestSplitVar)
        {
            if(cCurrentMissingN > 0)
            {
                dBestMissingSumZ   = dCurrentMissingSumZ;
                dBestMissingTotalW = dCurrentMissingTotalW;
                cBestMissingN      = cCurrentMissingN;
            }
            else // DEBUG: consider a weighted average with parent node?
            {
                dBestMissingSumZ   = dInitSumZ;
                dBestMissingTotalW = dInitTotalW;
                cBestMissingN      = 0;
            }
        }

        return hr;
    }



    double ThisNodePrediction() {return pThisNode->dPrediction;}
    bool operator<(const CNodeSearch &ns) {return dBestImprovement<ns.dBestImprovement;}

    unsigned long iBestSplitVar;
    double dBestSplitValue;

    double dBestLeftSumZ;
    double dBestLeftTotalW;
    unsigned long cBestLeftN;

    double dBestRightSumZ;
    double dBestRightTotalW;
    unsigned long cBestRightN;

    double dBestMissingSumZ;
    double dBestMissingTotalW;
    unsigned long cBestMissingN;

    double dCurrentMissingSumZ;
    double dCurrentMissingTotalW;
    unsigned long cCurrentMissingN;

    long cCurrentVarClasses;

    unsigned long iRank;
    double dInitTotalW;
    double dInitSumZ;
    unsigned long cInitN;
    double dBestImprovement;

private:
    bool fIsSplit;

    unsigned long cMinObsInNode;

    long cBestVarClasses;

    double dCurrentLeftSumZ;
    double dCurrentLeftTotalW;
    unsigned long cCurrentLeftN;
    double dCurrentRightSumZ;
    double dCurrentRightTotalW;
    unsigned long cCurrentRightN;
    double dCurrentImprovement;
    unsigned long iCurrentSplitVar;
    double dCurrentSplitValue;

    double dLastXValue;

    double *adGroupSumZ;
    double *adGroupW;
    unsigned long *acGroupN;
    double *adGroupMean;
    int *aiCurrentCategory;
    unsigned long *aiBestCategory;
    const unsigned long k_cMaxClasses;

    CNodeTerminal *pThisNode;
    CNode **ppParentPointerToThisNode;
    CNodeFactory *pNodeFactory;
};

typedef CNodeSearch *PCNodeSearch;
















#endif /* ART5C_NODE_SEARCH_HPP_ */
