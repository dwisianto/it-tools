/*
 * art5b_node_factory.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef ART5B_NODE_FACTORY_HPP_
#define ART5B_NODE_FACTORY_HPP_

#include <stack>
#include <list>
#include "gbf1a_common.hpp"
#include "art5d_node_terminal.hpp"
#include "art5f_node_continuous.hpp"
//#include "artnode_categorical.h"

#define NODEFACTORY_NODE_RESERVE ((unsigned long)100)

using namespace std;

class CNodeFactory {

public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("NodeFactory"));

    CNodeFactory() {}

    ~CNodeFactory() {
    	mLog.info("destructing node factory");
    }


    HRESULT Initialize(unsigned long cDepth) {

        HRESULT hr = S_OK;
        unsigned long i = 0;
        mLog.info("initilized");

        for(i=0; i<NODEFACTORY_NODE_RESERVE; i++)
        {
            TerminalStack.push(&(aBlockTerminal[i]));
            ContinuousStack.push(&(aBlockContinuous[i]));
            //CategoricalStack.push(&(aBlockCategorical[i]));
        }

        return hr;
    }

    CNodeTerminal* GetNewNodeTerminal()
    {
        if(TerminalStack.empty())
        {
        	mLog.info("Terminal stack is empty\n");
            pNodeTerminalTemp = NULL;
        }
        else
        {
            pNodeTerminalTemp = TerminalStack.top();
            TerminalStack.pop();

            pNodeTerminalTemp->dPrediction = 0.0;
        }
        return pNodeTerminalTemp;
    }

    CNodeContinuous* GetNewNodeContinuous() {

        if(ContinuousStack.empty())
        {
        	mLog.info("GetNewNodeContinuous::Continuous stack is empty");
            pNodeContinuousTemp = NULL;
        }
        else
        {
        	mLog.info("GetNewNodeContinuous::not empty");
            pNodeContinuousTemp = ContinuousStack.top();
            ContinuousStack.pop();

            pNodeContinuousTemp->dPrediction = 0.0;
            pNodeContinuousTemp->dImprovement = 0.0;
            pNodeContinuousTemp->pMissingNode = NULL;
            pNodeContinuousTemp->pLeftNode = NULL;
            pNodeContinuousTemp->pRightNode = NULL;
            pNodeContinuousTemp->iSplitVar = 0;
            pNodeContinuousTemp->dSplitValue = 0.0;
        }

        return pNodeContinuousTemp;
    }




    HRESULT RecycleNode(CNodeTerminal *pNode)
    {
        if(pNode != NULL)
        {
            TerminalStack.push(pNode);
        }
        return S_OK;
    }

    HRESULT RecycleNode(CNodeContinuous *pNode)
    {
        if(pNode != NULL)
        {
            if(pNode->pLeftNode != NULL) pNode->pLeftNode->RecycleSelf(this);
            if(pNode->pRightNode != NULL) pNode->pRightNode->RecycleSelf(this);
            if(pNode->pMissingNode != NULL) pNode->pMissingNode->RecycleSelf(this);
            ContinuousStack.push(pNode);
        }
        return S_OK;
    }





    //CNodeCategorical* GetNewNodeCategorical();
    //HRESULT RecycleNode(CNodeCategorical *pNode);

private:
    stack<PCNodeTerminal> TerminalStack;
    stack<PCNodeContinuous> ContinuousStack;
    //stack<PCNodeCategorical> CategoricalStack;

    CNodeTerminal* pNodeTerminalTemp;
    CNodeContinuous* pNodeContinuousTemp;
    //CNodeCategorical* pNodeCategoricalTemp;

    CNodeTerminal aBlockTerminal[NODEFACTORY_NODE_RESERVE];
    CNodeContinuous aBlockContinuous[NODEFACTORY_NODE_RESERVE];
    //CNodeCategorical aBlockCategorical[NODEFACTORY_NODE_RESERVE];
};

#endif /* ART5B_NODE_FACTORY_HPP_ */
