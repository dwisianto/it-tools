
/**
 *
 * print an array
 * sorting array and return its index
 * randomly generate a dataset
 * generate dataset based on id and row information
 *
 */
#ifndef GBF_UTIL_HPP_
#define GBF_UTIL_HPP_

#include "gbf1a_common.hpp"

class GBFUtil {
	public:
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("GBFUtil"));

	/*
	 * print an array
	 */
	template <typename T>
	void db_print_array( T * pa, int iLen) {
		mLog.debug("db_print_array");
		std::vector<T> va(pa,pa+iLen);
		std::copy(va.begin(), va.end(), std::ostream_iterator<T>(std::cout, " "));
		std::cout << std::endl;
	}

	template <typename T>
	void db_print_array_log(T* pa, int iLen) {

		string sArr = "";
		for(int i=0; i<iLen; i++) {
			sArr += std::to_string(pa[i])+" ";
		}
		mLog.info("db_print_array_log iLen %d", iLen);
		mLog.info("%s",sArr.c_str());

	}

	template <typename T>
	void db_print_array_log(T* pa, int iRow, int iCol) {

		int iLen = iRow*iCol;
		string sArr = "";
		for(int i=0; i<iLen; i++) {

			if( i % iCol == 0) {
				mLog.debug("%s",sArr.c_str());
				sArr = "";
			}

			sArr += std::to_string(pa[i])+" ";
		}

	}

	/*
	void db_print_array_log(double * pa, int iLen) {

		string sArr = "";
		for(int i=0; i<iLen; i++) {
			sArr += std::to_string(pa[i])+" ";
		}
		mLog.info("%s",sArr.c_str());

	}

	void db_print_order_log(int * pi, int iLen) {

		string sArr = "";
		for(int i=0; i<iLen; i++) {
			sArr += std::to_string(pi[i])+" ";
		}
		mLog.info("%s",sArr.c_str());

	}
	*/

	/*
	 * pa : array
	 * pi : index
	 * iLen : length of array
	 */
	void db_print_array_sorted_log(double *pa, int *pi, int iRow, int iCol) {

		string sArr = "";
		for(int i=0; i<iCol; i++) {
			int   * iArr = new int[iRow];
			double* dArr = new double[iRow];
			for(int j=0; j<iRow; j++) {
				int iTmp = i*iRow+j;
				dArr[j] = pa[iTmp];
				iArr[j] = pi[iTmp];
			}
			sArr = "";
			for(int j=0; j<iRow; j++) {
				sArr += std::to_string(dArr[iArr[j]])+" ";
				if( j%iCol == 0 ) {
					mLog.debug("%s",sArr.c_str());
					sArr="";
				}
			}

		}

	}



	/**
	 * sorting an array
	 */
	template <typename T>
	std::vector<size_t> sort_indexes(const std::vector<T> &v) {

	  // initialize original index locations
	  std::vector<size_t> idx(v.size());
	  std::iota(idx.begin(), idx.end(), 0);

	  // sort indexes based on comparing values in v
	  std::sort(idx.begin(), idx.end(),
	       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

	  return idx;
	}



	/**
	 */
	void db_sort_array( double * adX, int iLen,  int* iOutIdx ) {
	  std::vector<double> vdF(adX,adX+iLen);
	  std::vector<size_t> idx = sort_indexes(vdF);
	  // for (auto i: idx) { std::cout << i << std::endl; }
	  // size_t *iOutIdx = &idF[0];

	  for(int i=0; i<idx.size(); i++) {
	    iOutIdx[i] = static_cast<int>( idx[i]);
	  }
	}

	/**
	 *
	 */
	void db_sort_mat_o_idx(double *dX, int iRow, int iCol, int* piOutIdx) {

		//printf("iRow:%d iCol:%d \n",iRow,iCol);
		for(int i=0; i<iCol; i++) {
			// [] get an array of values
			double* dTmp = new double[iRow];
			for(int j=0; j<iRow; j++) {
				dTmp[j] = dX[i*iRow+j];
			}

			// [] do sorting
			int* piTmpIdx = new int[iRow];
			db_sort_array(dTmp, iRow,  piTmpIdx );
			// [] copy to output
			for(int j=0; j<iRow; j++) {
				piOutIdx[i*iRow+j]=piTmpIdx[j];
			}
		}

	}

	void db_init46(double * adX, double * adY, double * adWeight) {

	  adY[0] = 1;
	  adY[1] = 0;
	  adY[2] = 1;
	  adY[3] = 0;

	  adWeight[0] = 1;
	  adWeight[1] = 1;
	  adWeight[2] = 1;
	  adWeight[3] = 1;

	   adX[0] = 0.11;
	   adX[1] = 0.13;
	   adX[2] = 0.12;
	   adX[3] = 0.14;

	   adX[4] = 0.28;
	   adX[5] = 0.26;
	   adX[6] = 0.27;
	   adX[7] = 0.29;

	   adX[8] = 0.31;
	   adX[9] = 0.33;
	   adX[10] = 0.34;
	   adX[11] = 0.32;

	   adX[12] = 0.46;
	   adX[13] = 0.48;
	   adX[14] = 0.49;
	   adX[15] = 0.47;

	   adX[16] = 0.51;
	   adX[17] = 0.53;
	   adX[18] = 0.54;
	   adX[19] = 0.52;

	   adX[20] = 0.66;
	   adX[21] = 0.68;
	   adX[22] = 0.69;
	   adX[23] = 0.67;

	}


	void db_init64(double * adX, double * adY, double * adWeight) {

	  adY[0] = 1;
	  adY[1] = 0;
	  adY[2] = 1;
	  adY[3] = 0;
	  adY[4] = 1;
	  adY[5] = 0;

	  adWeight[0] = 1;
	  adWeight[1] = 1;
	  adWeight[2] = 1;
	  adWeight[3] = 1;
	  adWeight[4] = 1;
	  adWeight[5] = 1;

	   adX[0] = 0.11;
	   adX[1] = 0.13;
	   adX[2] = 0.12;
	   adX[3] = 0.14;
	   adX[4] = 0.18;
	   adX[5] = 0.16;

	   adX[6] = 0.27;
	   adX[7] = 0.29;
	   adX[8] = 0.21;
	   adX[9] = 0.23;
	   adX[10] = 0.24;
	   adX[11] = 0.22;

	   adX[12] = 0.36;
	   adX[13] = 0.38;
	   adX[14] = 0.39;
	   adX[15] = 0.37;
	   adX[16] = 0.51;
	   adX[17] = 0.53;

	   adX[18] = 0.44;
	   adX[19] = 0.42;
	   adX[20] = 0.46;
	   adX[21] = 0.48;
	   adX[22] = 0.49;
	   adX[23] = 0.47;

	}



	void db_fromFile_try() {

		// []
		std::string sName="../../r_prog/pk/gbm/gbm0600d/dgbm0600d/dat/letor1/r1a.txt";
		// [] number of observation
		int iObsLen = db_num_lines(sName);
		int iFeatLen = db_num_features(sName);
		cout << iObsLen << " x "<< iFeatLen << endl;

		// []
		double *aY = new double[iObsLen];
		double *aQ = new double[iObsLen];
		double *aW = new double[iObsLen];
		double *aX = new double[iObsLen*iFeatLen];
		//std::vector<int> dY;
		//std::vector<int> dQ;
		//std::vector<std::vector<double>> dX (iObsLen, std::vector<double>(iFeatLen) );

		db_fromFile(sName, iObsLen, iFeatLen, aY, aQ, aW, aX);
		cout << db_toString(iObsLen, iFeatLen, aX, aY, aW, aQ) << endl;
	}

	/**
	 * read data from a file name
	 * create
	 *  aY : relevance levels
	 *  aW : weight
	 *  aQ : query id
	 *  aX : feature matrix
	 */
	void db_fromFile(std::string sName, int iObsLen, int iFeatLen,
			double *adY,  double *adQ, double *adW, double *adX) {

		// []
		std::ifstream infile(sName);
		std::string aLine; // aLine from an input file
		std::string ssChunk; // temporary string
		std::string ssKey; // key
		std::string ssVal; // value

		for ( int iCtrObs=0; std::getline(infile, aLine); iCtrObs++ ) {

			// [] output the line
			//std::cout << aLine << std::endl;

			// [] weight
			adW[iCtrObs] = 1.0;

			// [] Relevance levels
			stringstream ssLine(aLine);
			getline(ssLine, ssChunk, ' ');
			adY[iCtrObs] = std::stod(ssChunk);
			//dY.push_back(std::stoi(ssChunk));
			//cout << std::stoi(ssChunk) <<endl;

			// QID
			getline(ssLine, ssChunk, ' ');
			stringstream ssQid(ssChunk);
			getline(ssQid, ssKey, ':');
			getline(ssQid, ssVal, ':');
			adQ[iCtrObs] = std::stod(ssVal);
			//dQ.push_back(std::stoi(ssVal));
			//cout << std::stoi(ssVal) <<endl;

			//std::vector<double> dXInst;
			for( int iCtrFeat=0; iCtrFeat<iFeatLen; iCtrFeat++ ) {
				getline(ssLine, ssChunk, ' ');
				stringstream ssQid(ssChunk);
				getline(ssQid, ssKey, ':');
				getline(ssQid, ssVal, ':');
				//cout << ssVal << endl;
				//cout << iCtrFeat*iObsLen+iCtrObs << endl;
				adX[iCtrFeat*iObsLen+iCtrObs] =std::stod(ssVal);
				//cout  << iCtrFeat*iObsLen+iCtrObs<< endl ;
				//cout << adX[iCtrFeat*iObsLen+iCtrObs] << endl;
				//dXInst.push_back(std::stod(ssVal));
			}

			//dX.push_back(dXInst);
		}

		// []
		//std::copy(dQ.begin(), dQ.end(), std::ostream_iterator<int>(std::cout, " "));
		//std::copy(dY.begin(), dY.end(), std::ostream_iterator<int>(std::cout, " "));

	}

	/**
	 * count the number of lines in a data file
	 */
	int db_num_lines(std::string sName) {

		std::ifstream infile(sName);

		int iCtr=0;
		std::string aLine;
		while (std::getline(infile, aLine)) {
			iCtr++;
		}

		return iCtr;
	}


	/**
	 * return number of observation
	 */
	int db_num_features(std::string sName) {

		int iCtr=0;
		std::string aLine; // aLine from an input file
		std::string ssChunk; // temporary string

		// [] open the file
		std::ifstream infile(sName);

		// [] get the first line
		std::getline(infile, aLine);

		stringstream ssLine(aLine);
		while( getline(ssLine, ssChunk, ' ') ) {
			if( ssChunk.find("#") == 0 ) {
				break;
			} else {
				iCtr++;
			}
		}

		return iCtr-2;
	}

	/*
	 * print out data information
	 */
	const char* db_toString(int iObsLen, int iFeatLen, double *adX, double *adY, double *adW, double *adQ) {

		std::string sOut = "";

		// []
		for(int iCtrObs=0; iCtrObs<iObsLen; iCtrObs++) {
			sOut = sOut + std::to_string(adY[iCtrObs])+ " ";
			sOut = sOut + std::to_string(adQ[iCtrObs])+ " ";
			sOut = sOut + std::to_string(adW[iCtrObs])+ " ";
			for( int iCtrFeat=0; iCtrFeat<iFeatLen; iCtrFeat++) {
				sOut = sOut + std::to_string( adX[iCtrFeat*iObsLen+iCtrObs]  ) + " ";
			}
			sOut = sOut + "\n";
		}

		return sOut.c_str();
	}

	/**
	 *
	 */
	void db_save(int iSizeObs, int iSizeFeat, double *adX, double *adY, double *adQ, std::string sName) {

		// [] open a file
		std::ofstream myfile;
		myfile.open (sName);

		// [] print out
		for(int iCtrObs=0; iCtrObs<iSizeObs; iCtrObs++) {
			std::string sOut="";
			sOut = sOut + std::to_string((int) adY[iCtrObs])+" qid:";
			sOut = sOut + std::to_string( adQ[iCtrObs])+" ";
			for(int iCtrFeat=0; iCtrFeat<iSizeFeat; iCtrFeat++) {
				sOut = sOut + std::to_string(iCtrFeat+1) + ":" + std::to_string(adX[iCtrFeat*iSizeObs+iCtrObs])+" ";
			}
			sOut = sOut + "#docid = "+ std::to_string(iCtrObs) + "\n";
			// write a file
			myfile << sOut;

		}
		//mLog.info("%s",sOut.c_str());
		myfile.close();

	}

	void db_save_small(int iSizeObs, int iSizeFeat, double *adX, double *adY, double *adQ, std::string sName) {

		// [] print out
		std::string sOut="";
		for(int iCtrObs=0; iCtrObs<iSizeObs; iCtrObs++) {
			sOut = sOut + std::to_string((int) adY[iCtrObs])+" qid:";
			sOut = sOut + std::to_string( adQ[iCtrObs])+" ";
			for(int iCtrFeat=0; iCtrFeat<iSizeFeat; iCtrFeat++) {
				sOut = sOut + std::to_string(iCtrFeat+1) + ":" + std::to_string(adX[iCtrFeat*iSizeObs+iCtrObs])+" ";
			}
			sOut = sOut + "#docid = "+ std::to_string(iCtrObs) + "\n";
		}
		//mLog.info("%s",sOut.c_str());

		std::ofstream myfile;
		myfile.open (sName);
		myfile << sOut;
		myfile.close();

	}



	void utl_init_array_to_zeros(int *dArray, int iSize) {
		for(int cCtr=0; cCtr<iSize; cCtr++) {
			dArray[cCtr] = 0;
		}
	}

	void utl_init_array_to_zeros(double *dArray, int iSize) {
		for(int cCtr=0; cCtr<iSize; cCtr++) {
			dArray[cCtr] = 0.0;
		}
	}


};

#endif
