/*
 * gbf1a_base.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef GBF1D_ROUTINE_HPP_
#define GBF1D_ROUTINE_HPP_

#include "gbf1a_common.hpp"
#include "gbf1e_utilities.hpp"

class GBFRoutine {

	public:

	// []
	//std::string initFileName = "log4cpp.properties";
	//log4cpp::PropertyConfigurator::configure(initFileName);
	//log4cpp::PropertyConfigurator::configure("log4cpp.properties");
	log4cpp::Category& mLogRoot = log4cpp::Category::getRoot();
	log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("GBFRoutine"));

	// []
	GBFUtil* mGbfUtl = new GBFUtil();

	// []
	void t1a_print_vector() {

		mLog.debug("t1a_print_vector");

		int cRows=4;
		int cCols=1;
		int cTrees=1; // number of trees to build

		double *adX = new double[cRows*cCols];
		adX[0] = 0.1;
		adX[1] = 0.2;
		adX[2] = 0.3;
		adX[3] = 0.4;

		vector<double> vdF(adX,adX+3);
		//std::vector<double> vdF(adX,adX+(sizeof(adX)/ sizeof(adX[0])));
		//std::string         sdF(vdF.begin(), vdF.end());
		//std::cout << "sdF"<< sdF << std::endl;
		copy(vdF.begin(), vdF.end(), ostream_iterator<double>(cout, " "));

	}

	void t1b_sort_index_array() {

		int cRows=4;
		int cCols=1;

		double *adX = new double[cRows*cCols];
		adX[0] = 0.1;
		adX[1] = 0.3;
		adX[2] = 0.2;
		adX[3] = 0.4;


		mGbfUtl->db_print_array(adX, cRows*cCols);

		int* iIdx  = new int[cRows];
		mGbfUtl->db_sort_array( adX, cRows*cCols, iIdx );
		mGbfUtl->db_print_array( iIdx, cRows*cCols);
		mGbfUtl->db_print_array_log(adX, cRows*cCols);
		mGbfUtl->db_print_array_sorted_log(adX, iIdx, cRows,cCols);
	}

	/**
	 * verify that ordering is correct
	 */
	void t1c_sort_index_matrix() {

		int cRows=5;
		int cCols=2;

		//X1 <- c(0.6,0.4,0.2,0.8,1.0)
		//X2 <- c(0.3,0.9,0.5,0.1,0.7)
		//Y <- c(1,2,3,4,5)
		//data <- data.frame(Y=Y,X1=X1,X2=X2)
		double adX[10]   = {0.6,0.4,0.2,0.8,1.0,0.3,0.9,0.5,0.1,0.7};
		double aiXgt[10] = {2,1,0,3,4,3,0,2,4,1};

		// sort and get index
		int *aiXOrder = new int[cRows*cCols];
		mGbfUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
		mGbfUtl->db_print_array(adX,cRows*cCols);
		mGbfUtl->db_print_array(aiXOrder,cRows*cCols);
		mGbfUtl->db_print_array_log(adX, cRows*cCols);
		mGbfUtl->db_print_array_sorted_log(adX, aiXOrder, cRows,cCols);

	}

	void t1d_init_data46() {

	   int cRows=4;
	   int cCols=6;

	   double *adX = new double[cRows*cCols];
	   double *adY = new double[cRows];
	   double *adW = new double[cRows];
	   mGbfUtl->db_init46( adX, adY, adW);
	   mGbfUtl->db_print_array(adX,cRows*cCols);

	   // sort and get index
	   int *aiXOrder = new int[cRows*cCols];
	   mGbfUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	   mGbfUtl->db_print_array_log(adX, cRows*cCols);
	   mGbfUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);

	}


	void t1e_init_data46() {

	  // [] Data :: Constants
	  int cRows  = 4;
	  int cCols  = 6;
	  int cTrain = 3;
	  double dBagFraction = 0.5;

	  // [] Trees ::
	  int cTrees          = 1; // number of trees to build
	  int cLeavesDepth    = 1;
	  int cMinObsInNode   = 1;
	  double dShrinkage   = 0.5;

	  // [] Data :: Structures
	  char pszFamily []   = "gaussian";
	  CDistribution *pDist = NULL;

	  CDataset *pData      = new CDataset();

	  double *adY      = new double[cRows];
	  double *adWeight = new double[cRows];

	  double *adX      = new double[cRows*cCols];
	  int *aiXOrder    = new int[cRows*cCols];

	  double *adOffset = NULL;
	  double *adMisc   = NULL;

	  int *acVarClasses   = new int[cCols];
	  int *alMonotoneVar  = new int[cCols];

	  // [] initialize values
	  for(int cCtr=0; cCtr<cCols; cCtr++) {
	    acVarClasses[cCtr] = 0;
	    alMonotoneVar[cCtr] = 0;
	  }
	  mGbfUtl->db_init46(adX, adY, adWeight);
	  mGbfUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
	  //mGbfUtl->db_print_array(aiXOrder,cRows*cCols);
	  mGbfUtl->db_print_array_log(adX, cRows*cCols);
	  mGbfUtl->db_print_array_log(aiXOrder, cRows*cCols);
	  mGbfUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);

	}

	void t1f_db_read_try() {

		//mGbmUtl->db_read(sFile);
		// []

		//r2xTst * tt = new r2xTst();
		//tt->mGbmUtl->db_read(sFile);
		mGbfUtl->db_fromFile_try();

	}

	void t3a(gbf_settings cfg) {

		// [] Cfg : Result
		std::string sLgMdlTreeNameDot = cfg.m_resultTreeDot;// "logs/dotMdlTree";
		std::string sLgPrdTree        = cfg.m_resultTreePred; //"logs/prdMdlTree";
		std::string sLgPrdFrst        = cfg.m_resultForestPred; //"logs/prdMdlFrst";

		// [] Cfg : Data : Constants
		int cRows                   = cfg.m_crows;
		int cCols                   = cfg.m_ccols;
		std::string sDbNameOriginal = cfg.m_dbloc; //"../dat/letor1/r1a.txt";

		int cTrain          = cRows;
		double dBagFraction = 1.0;

		// [] Trees ::
		int cTrees          = cfg.m_iMdlForestSize; // number of trees to build
		double dShrinkage   = cfg.m_dMdlForestShrinkage;
		double dLambda      = cfg.m_dMdlForestLambda; // Is dLambda the same as dShrinkage

		int cLeavesDepth    = cfg.m_iMdlTreeDepth;
		int cMinObsInNode   = cfg.m_iMdlTreeMinObs;

		mLogRoot.alert(" Row x Cols : %d x %d ",cfg.m_crows,cfg.m_ccols);
		mLogRoot.alert(" Db Name: %s, Loc: %s",cfg.m_dbname.c_str(), cfg.m_dbloc.c_str());
		mLogRoot.alert(" Result %s %s",cfg.m_result_loc.c_str(), cfg.m_log_loc.c_str());

		mLogRoot.alert(" Forest Size: %d Shrinkage: %f lambda: %f", cTrees, dShrinkage, dLambda);
		mLogRoot.alert(" Tree Depth: %d MinObs: %d", cLeavesDepth,cMinObsInNode);


		// [] Data :: Structures
		int cNodes = 0;
		char pszFamily []    = "gaussian";
		CDistribution *pDist = NULL;

		CDataset *pData      = new CDataset();

		double *adY          = new double[cRows];
		double *adWeight     = new double[cRows];

		double *adX          = new double[cRows*cCols];
		int *aiXOrder        = new int[cRows*cCols];

		double *adQ          = new double[cRows];
		double *adF          = new double[cRows]; // predictions

		double *adOffset = NULL;
		double *adMisc   = NULL;

		int *acVarClasses   = new int[cCols];
		int *alMonotoneVar  = new int[cCols];



		// [] initialize values
		//for(int iCtr=0; iCtr<cRows; iCtr++) {		   adF[iCtr] = 0.0;	   }
		// for(int cCtr=0; cCtr<cCols; cCtr++) { acVarClasses[cCtr] = 0; alMonotoneVar[cCtr] = 0; }
		mGbfUtl->utl_init_array_to_zeros(adF, cRows);
		mGbfUtl->utl_init_array_to_zeros(acVarClasses, cCols);
		mGbfUtl->utl_init_array_to_zeros(alMonotoneVar, cCols);

		// [] load the dataset
		int iLenObs=mGbfUtl->db_num_lines(sDbNameOriginal);
		int iLenFeat=mGbfUtl->db_num_features(sDbNameOriginal);
		mLog.info(" sDbName %s ,iLenObs %d ,iLenFeat %d", sDbNameOriginal.c_str(), iLenObs, iLenFeat);
		mGbfUtl->db_fromFile(sDbNameOriginal, iLenObs, iLenFeat, adY, adQ, adWeight, adX);
		mGbfUtl->db_save(iLenObs, iLenFeat, adX, adY, adQ,sDbNameOriginal+".tmp" );
		//mGbfUtl->db_init204(adX, adY, adWeight);

		// sort the feature matrix
		mGbfUtl->db_sort_mat_o_idx(adX, cRows, cCols, aiXOrder);
		mGbfUtl->db_print_array_log(adX, cRows, cCols);
		mGbfUtl->db_print_array_log(aiXOrder, cRows, cCols);
		mGbfUtl->db_print_array_sorted_log(adX, aiXOrder, cRows, cCols);

		// initialize some things
		gbm_setup(adY,
				adOffset,
				adX,
				aiXOrder,
				adWeight,
				adMisc,
				cRows,
				cCols,
				acVarClasses,
				alMonotoneVar,
				pszFamily,
				cTrees,
				cLeavesDepth,
				cMinObsInNode,
				dShrinkage,
				dBagFraction,
				cTrain,
				pData,
				pDist);

		   // [] mLog.info(" initialize ");
		   double dNumTrain = cTrain;
		   //double dBagFraction =0.75;
		   //unsigned long cLeaveDepth = 3;
		   //unsigned long cMinsObsInNode = 3;
		   CGBM *pGBM = new CGBM();
		   pGBM->Initialize(pData,
				    pDist,
				    dLambda,
				    dNumTrain,
				    dBagFraction,
				    cLeavesDepth,
				    cMinObsInNode);


		   CMdlForest *dMdlFrst = new CMdlForest();
		   dMdlFrst->initialize(cTrees);
		   for(int iCtr=0; iCtr<cTrees; iCtr++) {
			   mLog.alert("iCtr %d", iCtr);

			   double dTrainError = 0.0;
			   double dValidError = 0.0;
			   double dOOBagImprove = 0.0;

			   pGBM->iterate(adF,
			                 dTrainError,
			                 dValidError,
			                 dOOBagImprove,
			                 cNodes);

				 // []
			     std::string sFileDot=sLgMdlTreeNameDot+std::to_string(iCtr);
			     //mLog.info(" dotMdlTree %s ", sFileDot.c_str());
			     CMdlTree *aTree = new CMdlTree();
			     aTree->initialize(pGBM, cNodes);
			     aTree->write_dots(sFileDot);
			     //mLog.info( aTree->toString() );

			     std::string sFilePrd=sLgPrdTree+std::to_string(iCtr);
			     aTree->predictAndSave( cTrain, adY, adX, sFilePrd );

			     // [] Forest insertion
			     dMdlFrst->insert(iCtr,aTree);
		   }
		   mLogRoot.alert(" sLgPrdFrst %s ",sLgPrdFrst.c_str() );
		   dMdlFrst->predictAndSave( cTrain, cTrees, adY, adX,sLgPrdFrst );

		   mLog.alert("TERMINATE");
	}


	/*
	void t2a_gbm_run();
	void t2b_gbm_run();

	//void t2c_gbf_run();
	void t2c_gbf(gbf_settings );

	void t4a_print();

	void t5a_plot();
	*/
};

#endif /* GBF1D_ROUTINE_HPP_ */
