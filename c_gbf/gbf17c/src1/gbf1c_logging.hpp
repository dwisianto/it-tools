/*
 * gbf1a_logging.hpp
 *
 *  Created on: Jun 2, 2017
 *      Author: dsm
 */

#ifndef GBF1C_LOGGING_HPP_
#define GBF1C_LOGGING_HPP_

// log4cpp
#define LOG4CPP_FIX_ERROR_COLLISION 1
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

#endif /* GBF1C_LOGGING_HPP_ */
