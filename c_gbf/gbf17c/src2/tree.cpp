//  GBM by Greg Ridgeway  Copyright (C) 2003
#include "tree.h"


CCARTTree::CCARTTree()
{
    pRootNode = NULL;
    pNodeFactory = NULL;
    dShrink = 1.0;
}


CCARTTree::~CCARTTree()
{
    if(pRootNode != NULL)
    {
        pRootNode->RecycleSelf(pNodeFactory);
    }
}


HRESULT CCARTTree::Initialize
(
    CNodeFactory *pNodeFactory
)
{
    HRESULT hr = S_OK;

    this->pNodeFactory = pNodeFactory;

    return hr;
}


HRESULT CCARTTree::Reset()
{
    HRESULT hr = S_OK;

    if(pRootNode != NULL)
    {
        // delete the old tree and start over
        hr = pRootNode->RecycleSelf(pNodeFactory);
    }
    if(FAILED(hr))
    {
        goto Error;
    }

    iBestNode = 0;
    dBestNodeImprovement = 0.0;

    schWhichNode = 0;

    pNewSplitNode    = NULL;
    pNewLeftNode     = NULL;
    pNewRightNode    = NULL;
    pNewMissingNode  = NULL;
    pInitialRootNode = NULL;

Cleanup:
    return hr;
Error:
    goto Cleanup;
}



//------------------------------------------------------------------------------
// Grows a regression tree
//------------------------------------------------------------------------------
HRESULT CCARTTree::grow
(
    double *adZ,
    CDataset *pData,
    double *adW,
    double *adF,
    unsigned long nTrain,
    unsigned long nBagged,
    double dLambda,
    unsigned long cMaxDepth,
    unsigned long cMinObsInNode,
    bool *afInBag,
    unsigned long *aiNodeAssign,
    CNodeSearch *aNodeSearch,
    VEC_P_NODETERMINAL &vecpTermNodes
)
{
    HRESULT hr = S_OK;
    mLog.info("grow - starts");


    if((adZ==NULL) || (pData==NULL) || (adW==NULL) || (adF==NULL) || (cMaxDepth < 1)) {
        hr = E_INVALIDARG;
        ErrorTrace(hr);
        goto Error;
    }

    dSumZ = 0.0;
    dSumZ2 = 0.0;
    dTotalW = 0.0;


    mLog.info("initial tree calcs");
    for(iObs=0; iObs<nTrain; iObs++) {
        // aiNodeAssign tracks to which node each training obs belongs
        aiNodeAssign[iObs] = 0;
        if(afInBag[iObs]) {
            // get the initial sums and sum of squares and total weight
            dSumZ += adW[iObs]*adZ[iObs];
            dSumZ2 += adW[iObs]*adZ[iObs]*adZ[iObs];
            dTotalW += adW[iObs];
            mLog.info("afInBag iObs %d sZ %f sZ2 %f tw %f ", iObs, dSumZ, dSumZ2, dTotalW);
        }
    }
    dError = dSumZ2-dSumZ*dSumZ/dTotalW;
    mLog.info("Growing tree dError %f", dError);

    pInitialRootNode = pNodeFactory->GetNewNodeTerminal();
    pInitialRootNode->dPrediction = dSumZ/dTotalW;
    pInitialRootNode->dTrainW = dTotalW;
    mLog.debug(" dPrediction %f ", dSumZ/dTotalW);
    mLog.debug("vecpTermNodes resize %lu %lu ",cMaxDepth, 2*cMaxDepth + 1);
    vecpTermNodes.resize(2*cMaxDepth + 1,NULL); // accounts for missing nodes
    mLog.debug("vecpTermNodes resize ");
    vecpTermNodes[0] = pInitialRootNode;
    mLog.debug("vecpTermNodes [0] initialized ");
    pRootNode = pInitialRootNode;

    aNodeSearch[0].Set(dSumZ,dTotalW,nBagged,
                       pInitialRootNode,
                       &pRootNode,
                       pNodeFactory);

    // build the tree structure
    mLog.info("Building tree ");
    cTotalNodeCount = 1;
    cTerminalNodes = 1;
    for(cDepth=0; cDepth<cMaxDepth; cDepth++) {
    	mLog.info("cDepth %lu / %lu ",cDepth, cMaxDepth);

        hr = GetBestSplit(pData,
                          nTrain,
                          aNodeSearch,
                          cTerminalNodes,
                          aiNodeAssign,
                          afInBag,
                          adZ,
                          adW,
                          iBestNode,
                          dBestNodeImprovement);
        mLog.info("iBestNode[ %lu ]= %f ",iBestNode, dBestNodeImprovement);
        if(dBestNodeImprovement == 0.0) {
        	mLog.info("ERROR::Breaking because dBestNodeImprovement is zero");
            break;
        }

        // setup the new nodes and add them to the tree
        mLog.info("grow:: SetupNewNodes ");
        hr = aNodeSearch[iBestNode].SetupNewNodes(pNewSplitNode,
                                                  pNewLeftNode,
                                                  pNewRightNode,
                                                  pNewMissingNode);
	mLog.debug(" SetupNewNodes::iBestNode %lu cTerminalNodes %lu ", iBestNode, cTerminalNodes);
        cTotalNodeCount += 3;
        cTerminalNodes += 2;
	vecpTermNodes[iBestNode] = pNewLeftNode;
	vecpTermNodes[cTerminalNodes-2] = pNewRightNode;
	vecpTermNodes[cTerminalNodes-1] = pNewMissingNode;
	mLog.debug(" cTot %lu cTerm %lu ",cTotalNodeCount,cTerminalNodes );




    // assign observations to the correct node
	mLog.debug(" assign observations to the correct node iBestNode %lu ", iBestNode);
        for(iObs=0; iObs < nTrain; iObs++) {
            iWhichNode = aiNodeAssign[iObs];
            if(iWhichNode==iBestNode) {
                schWhichNode = pNewSplitNode->WhichNode(pData,iObs);
                if(schWhichNode == 1) { // goes right
                    aiNodeAssign[iObs] = cTerminalNodes-2;
                    mLog.debug(" goes right %lu", aiNodeAssign[iObs]);
                } else if(schWhichNode == 0) { // is missing
                    aiNodeAssign[iObs] = cTerminalNodes-1;
                    mLog.debug(" is missing  %lu", aiNodeAssign[iObs]);
                }
                // those to the left stay with the same node assignment
            }
        }


        // set up the node search for the new right node
        mLog.debug(" set up the node search for the new right node");
        aNodeSearch[cTerminalNodes-2].Set(aNodeSearch[iBestNode].dBestRightSumZ,
                                          aNodeSearch[iBestNode].dBestRightTotalW,
                                          aNodeSearch[iBestNode].cBestRightN,
                                          pNewRightNode,
                                          &(pNewSplitNode->pRightNode),
                                          pNodeFactory);
        // set up the node search for the new missing node
        mLog.debug(" set up the node search for the new missing node");
        aNodeSearch[cTerminalNodes-1].Set(aNodeSearch[iBestNode].dBestMissingSumZ,
                                          aNodeSearch[iBestNode].dBestMissingTotalW,
                                          aNodeSearch[iBestNode].cBestMissingN,
                                          pNewMissingNode,
                                          &(pNewSplitNode->pMissingNode),
                                          pNodeFactory);
        // set up the node search for the new left node
        // must be done second since we need info for right node first
        mLog.debug(" set up the node search for the new left node");
        aNodeSearch[iBestNode].Set(aNodeSearch[iBestNode].dBestLeftSumZ,
                                   aNodeSearch[iBestNode].dBestLeftTotalW,
                                   aNodeSearch[iBestNode].cBestLeftN,
                                   pNewLeftNode,
                                   &(pNewSplitNode->pLeftNode),
                                   pNodeFactory);

    } // end tree growing

    // DEBUG
    //Print();

Cleanup:
    return hr;
Error:
    goto Cleanup;
}


HRESULT CCARTTree::GetBestSplit
(
    CDataset *pData,
    unsigned long nTrain,
    CNodeSearch *aNodeSearch,
    unsigned long cTerminalNodes,
    unsigned long *aiNodeAssign,
    bool *afInBag,
    double *adZ,
    double *adW,
    unsigned long &iBestNode,
    double &dBestNodeImprovement
)
{
    HRESULT hr = S_OK;

    int iVar = 0;
    unsigned long iNode = 0;
    unsigned long iOrderObs = 0;
    unsigned long iWhichObs = 0;
    unsigned long cVarClasses = 0;
    double dX = 0.0;

    mLog.info("CCARTTree::GetBestSplit::Starts");
    for(iVar=0; iVar < pData->cCols; iVar++) {

      cVarClasses = pData->acVarClasses[iVar];
      mLog.info("   iVar %d cVarClasses %lu ",iVar, cVarClasses);
      for(iNode=0; iNode < cTerminalNodes; iNode++) {
	hr = aNodeSearch[iNode].ResetForNewVar(iVar,cVarClasses);
      }

      // distribute the observations in order to the correct node search
      for(iOrderObs=0; iOrderObs < nTrain; iOrderObs++) {
	mLog.info("    iOrderObs %lu nTrain %lu  ", iOrderObs, nTrain);
	iWhichObs = pData->aiXOrder[iVar*nTrain + iOrderObs];
	if(afInBag[iWhichObs]) {
	  iNode = aiNodeAssign[iWhichObs];
	  dX = pData->adX[iVar*(pData->cRows) + iWhichObs];
	  mLog.debug("     iNode %lu    dX %f iWhichObs %lu", iNode, dX, iWhichObs);
	  mLog.info("      adZ %f adW %f mono %lu ",adZ[iWhichObs],adW[iWhichObs],pData->alMonotoneVar[iVar]);

	  hr = aNodeSearch[iNode].IncorporateObs
                     (dX,
                      adZ[iWhichObs],
                      adW[iWhichObs],
                      pData->alMonotoneVar[iVar]);


	}

      }

      for(iNode=0; iNode<cTerminalNodes; iNode++) {
	/*
	if(cVarClasses != 0) { // evaluate if categorical split
	  hr = aNodeSearch[iNode].EvaluateCategoricalSplit();
	}
	*/
	aNodeSearch[iNode].WrapUpCurrentVariable();
      }
    }

    // search for the best split
    iBestNode = 0;
    dBestNodeImprovement = 0.0;
    for(iNode=0; iNode<cTerminalNodes; iNode++) {

      aNodeSearch[iNode].SetToSplit();
      mLog.info(" iNode %lu cTerminalNodes %lu improve %f dBest %f",
	     iNode, cTerminalNodes, aNodeSearch[iNode].BestImprovement(), dBestNodeImprovement );
      if( aNodeSearch[iNode].BestImprovement() > dBestNodeImprovement) {
	iBestNode = iNode;
	dBestNodeImprovement = aNodeSearch[iNode].BestImprovement();
      }
    }

    mLog.info(" iBestNode %lu  Improvement %f ", iBestNode, dBestNodeImprovement);
    mLog.info("CCARTTree::GetBestSplit::Ends");
    return hr;
}


HRESULT CCARTTree::GetNodeCount
(
    int &cNodes
)
{
    cNodes = cTotalNodeCount;
    return S_OK;
}



HRESULT CCARTTree::PredictValid
(
    CDataset *pData,
    unsigned long nValid,
    double *adFadj
)
{
    HRESULT hr = S_OK;
    int i=0;

    for(i=pData->cRows - nValid; i<pData->cRows; i++)
    {
        pRootNode->Predict(pData, i, adFadj[i]);
        adFadj[i] *= dShrink;
    }

    return hr;
}



HRESULT CCARTTree::Predict
(
    double *adX,
    unsigned long cRow,
    unsigned long cCol,
    unsigned long iRow,
    double &dFadj
)
{

    if(pRootNode != NULL)
    {
        pRootNode->Predict(adX,cRow,cCol,iRow,dFadj);
        dFadj *= dShrink;
    }
    else
    {
        dFadj = 0.0;
    }

    return S_OK;
}



HRESULT CCARTTree::Adjust
(
    unsigned long *aiNodeAssign,
    double *adFadj,
    unsigned long cTrain,
    VEC_P_NODETERMINAL &vecpTermNodes,
    unsigned long cMinObsInNode
)
{
    unsigned long hr = S_OK;
    unsigned long iObs = 0;

    hr = pRootNode->Adjust(cMinObsInNode);
    if(FAILED(hr))
    {
        goto Error;
    }

    // predict for the training observations
    for(iObs=0; iObs<cTrain; iObs++)
    {
        adFadj[iObs] = vecpTermNodes[aiNodeAssign[iObs]]->dPrediction;
    }

Cleanup:
    return hr;
Error:
    goto Cleanup;
}


HRESULT CCARTTree::Print()
{
    HRESULT hr = S_OK;

    if(pRootNode != NULL) {
        mLog.info("Print:shrinkage: %f",dShrink);
        mLog.info("Print:initial error: %f",dError);
        pRootNode->PrintSubtree(0);
    }

    return hr;
}



HRESULT CCARTTree::GetVarRelativeInfluence
(
    double *adRelInf
)
{
    HRESULT hr = S_OK;

    if(pRootNode != NULL)
    {
        hr = pRootNode->GetVarRelativeInfluence(adRelInf);
        if(FAILED(hr))
        {
            ErrorTrace(hr);
            goto Error;
        }
    }

Cleanup:
    return hr;
Error:
    goto Cleanup;

}



HRESULT CCARTTree::TransferTreeToRList
(
    CDataset *pData,
    int *aiSplitVar,
    double *adSplitPoint,
    int *aiLeftNode,
    int *aiRightNode,
    int *aiMissingNode,
    double *adErrorReduction,
    double *adWeight,
    VEC_VEC_CATEGORIES &vecSplitCodes,
    int cCatSplitsOld,
    double dShrinkage
)
{
    HRESULT hr = S_OK;

    int iNodeID = 0;

    if(pRootNode != NULL)
    {
        hr = pRootNode->TransferTreeToRList(iNodeID,
                                            pData,
                                            aiSplitVar,
                                            adSplitPoint,
                                            aiLeftNode,
                                            aiRightNode,
                                            aiMissingNode,
                                            adErrorReduction,
                                            adWeight,
                                            vecSplitCodes,
                                            cCatSplitsOld,
                                            dShrinkage);
    }
    else
    {
        hr = E_FAIL;
    }
    return hr;
}
