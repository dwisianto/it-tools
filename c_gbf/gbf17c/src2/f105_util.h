
#ifndef _F105_UTIL_H_
#define _F105_UTIL_H_

#include "buildinfo.h"
#include<string>

class CUtil {

public:

	CUtil();
	~CUtil();
    log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("CUtil"));

    void log_array_int(std::string sArr, int * pArr, int iLen);
    void log_array_double(std::string sArr, double * pArr, int iLen);

};

#endif /* _F105_UTIL_H_ */
