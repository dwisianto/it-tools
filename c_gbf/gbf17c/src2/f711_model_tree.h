/*
 * f711_model_tree.h
 *
 *  Created on: Apr 25, 2017
 *      Author: dsm
 */

#ifndef SRC_F711_MODEL_TREE_H_
#define SRC_F711_MODEL_TREE_H_

#include <iostream>
#include <fstream>

#include "buildinfo.h"
#include "gbm_engine.h"
#include "f105_util.h"

using namespace std;

class CMdlTree {

public:

	CMdlTree();
    ~CMdlTree();
    log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("mdlTree"));

    HRESULT initialize( CGBM *pGBM, int iSizeNode  );
    const char* toString( );

    HRESULT write_dots(std::string sOutDot);
    HRESULT predict( int iSizeObs, double *adX, double *adF );
    HRESULT predictAndSave( int iSizeObs, double *adY, double *adX, std::string sFileName );

    CUtil *pUtil = new CUtil();

    int miSizeNode;
	int *maiSplitVar;
	double *madSplitPoint;
	int *maiLeftNode;
	int *maiRightNode;
	int *maiMissingNode;
	double *madErrorReduction;
	double *madWeight;

	int miCatSplitsOld;

};

#endif /* SRC_F711_MODEL_TREE_H_ */
