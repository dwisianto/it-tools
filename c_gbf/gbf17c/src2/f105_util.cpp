#include "f105_util.h"

CUtil::CUtil()
{
}

CUtil::~CUtil()
{
}


/**
 * log an array
 */
void CUtil::log_array_double(std::string sArr, double * pArr, int iLen) {

	sArr += " ";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pArr[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

void CUtil::log_array_int(std::string sArr, int * pArr, int iLen) {

	sArr += " ";
	for(int i=0; i<iLen; i++) {
		sArr += std::to_string(pArr[i])+" ";
	}
	mLog.info("%s",sArr.c_str());

}

