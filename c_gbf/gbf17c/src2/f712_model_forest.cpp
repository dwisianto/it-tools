

#include "f712_model_forest.h"

CMdlForest::CMdlForest()
{
}

CMdlForest::~CMdlForest()
{
}

HRESULT CMdlForest::initialize( int iSizeTrees  ) {
	mTreesSize = iSizeTrees;
	mTrees = new CMdlTree*[iSizeTrees];
}

HRESULT CMdlForest::insert(int iCtr, CMdlTree* aMdlTree ) {
	mTrees[iCtr] = aMdlTree;
}

HRESULT CMdlForest::getSize(int iSize) {
	iSize = mTreesSize;
}

HRESULT CMdlForest::getTree(int iCtr, CMdlTree* aMdlTree) {
	aMdlTree = mTrees[iCtr];
}

const char * CMdlForest::toString() {

	std::string sOut = "iTreeSize:"+ std::to_string(mTreesSize)+"\n";
	for(int iCtr=0; iCtr<mTreesSize; iCtr++) {
		sOut = sOut + " iTree="+ std::to_string(iCtr);
		sOut = sOut + mTrees[iCtr]->toString();
		sOut = sOut + "\n";
	}

	return sOut.c_str();
}


/**
 * iSizeObs
 * adX
 * adPredF
 */
HRESULT CMdlForest::predict( int iSizeObs, double *adX, double *adPredF ) {

	unsigned long hr = 0;

	predict( iSizeObs, this->mTreesSize, adX, adPredF );

    Cleanup:
        return hr;
    Error:
        goto Cleanup;
}

/**
 * iSizeObs
 * adX
 * adPredF
 */
HRESULT CMdlForest::predict( int iSizeObs, int iSizeTrees, double *adX, double *adPredF ) {

	unsigned long hr = 0;

    int iObs = 0;    // counter for observations
    int iTree = 0;   // counter for trees

    int cRows = iSizeObs; // INTEGER(rcRows)[0];
    int cTrees = mTreesSize;// INTEGER(rcTrees)[0];

    CMdlTree *rThisTree = NULL;
    int *aiSplitVar = NULL;
    double *adSplitCode = NULL;
    int *aiLeftNode = NULL;
    int *aiRightNode = NULL;
    int *aiMissingNode = NULL; // don't consider missing values
    int iCurrentNode = 0;
    double dX = 0.0;
    //int iCatSplitIndicator = 0; // for categorical split

    //
    //SEXP radPredF = NULL;
    // allocate the predictions to return

    //int iCurrentNode = 0;
    //double* adPredF = new double[cRows];
    for(iObs=0; iObs<cRows; iObs++) {        adPredF[iObs] = 0;    }

    // Loop through trees
    for(iTree=0; iTree< iSizeTrees; iTree++) {
        rThisTree     = mTrees[iTree]; //VECTOR_ELT(rTrees,iTree);
        aiSplitVar    = mTrees[iTree]->maiSplitVar; // INTEGER(VECTOR_ELT(rThisTree,0));
        adSplitCode   = mTrees[iTree]->madSplitPoint; // REAL   (VECTOR_ELT(rThisTree,1));
        aiLeftNode    = mTrees[iTree]->maiLeftNode; //INTEGER(VECTOR_ELT(rThisTree,2));
        aiRightNode   = mTrees[iTree]->maiRightNode; //INTEGER(VECTOR_ELT(rThisTree,3));
        aiMissingNode = mTrees[iTree]->maiMissingNode; //INTEGER(VECTOR_ELT(rThisTree,4));
        for(iObs=0; iObs<cRows; iObs++) {
            iCurrentNode = 0;
            while(aiSplitVar[iCurrentNode] != -1) {
            	dX = adX[aiSplitVar[iCurrentNode]*cRows + iObs];
            	if(dX < adSplitCode[iCurrentNode]) {
            		iCurrentNode = aiLeftNode[iCurrentNode];
            	} else {
            		iCurrentNode = aiRightNode[iCurrentNode];
            	}

            }
            adPredF[iObs] += adSplitCode[iCurrentNode]; // add the prediction
        } // iObs
    } // iTree

    Cleanup:
    //UNPROTECT(1); // radPredF
        return hr;
    Error:
        goto Cleanup;
}

HRESULT CMdlForest::predictAndSave( int iSizeObs, int iSizeForest, double *adY, double *adX, std::string sOutFileFrst ) {

	for(int iCtrFrst=0; iCtrFrst< iSizeForest; iCtrFrst++) {
		std::string sOutFile = sOutFileFrst + std::to_string(iCtrFrst);
		std::ofstream myfile;
		myfile.open (sOutFile);

		double *adPred = new double[iSizeObs];
		predict( iSizeObs, (iCtrFrst+1) , adX, adPred);

		for(int i=0; i<iSizeObs; i++) {
			//sOut = sOut + "\t" + std::to_string(i);
			//sOut = sOut + "\t" + std::to_string(adY[i]);
			//sOut = sOut + "\t" + std::to_string(adPred[i]);
			myfile << std::to_string(adPred[i]) + "\n";
		}

		myfile.close();
	}

	unsigned long hr = 0;
    Cleanup:
    return hr;
    Error:
        goto Cleanup;
}



HRESULT CMdlForest::predictAndSave2( int iSizeObs, int iSizeForest, double *adY, double *adX, std::string sOutFileFrst ) {

	int iSize =iSizeObs*iSizeForest;
	double *adPredFrst = new double[iSize];
	std::fill_n(adPredFrst, iSize,  0); // filling zeros
	for(int iCtrFrst=0; iCtrFrst<iSizeForest; iCtrFrst++) {

		double *adPred = new double[iSizeObs];
		predict( iSizeObs, (iCtrFrst+1) , adX, adPred);

		std::copy(adPred, adPred+iSizeObs, adPredFrst+(iCtrFrst*iSizeObs));
		delete adPred;
	}
	//[dbg] vector<double> vdF(adPredFrst,adPredFrst+iSize);
	//[dbg] std::copy(vdF.begin(), vdF.end(), ostream_iterator<double>(cout, " "));


	// [] print out
	std::string sOut="";
	for(int iCtrObs=0; iCtrObs<iSizeObs; iCtrObs++) {
		sOut = sOut + "\t" + std::to_string(iCtrObs);
		sOut = sOut + "\t" + std::to_string(adY[iCtrObs]);
		for(int iCtrFrst=0; iCtrFrst<iSizeForest; iCtrFrst++) {
			sOut = sOut + "\t" + std::to_string(adPredFrst[iCtrFrst*iSizeObs+iCtrObs]);
		}
		sOut = sOut + "\n";
	}
	sOut+="\n";
	mLog.alert("%s",sOut.c_str());

	std::ofstream myfile;
	myfile.open (sOutFileFrst);
	myfile << sOut;
	myfile.close();


	unsigned long hr = 0;
    return hr;
}
