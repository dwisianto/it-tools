
#ifndef _F712_
#define _F712_

#include<iostream>
#include<iterator>

#include "buildinfo.h"
#include "f711_model_tree.h"


class CMdlForest {

public:

	CMdlForest();
	~CMdlForest();
    log4cpp::Category& mLog = log4cpp::Category::getInstance(std::string("CMdlForest"));

    HRESULT initialize( int iSizeNode  );
    HRESULT insert(int iCtr, CMdlTree* aMdlTree );
    HRESULT getSize(int iSize);
    HRESULT getTree(int iCtr, CMdlTree* aMdlTree);
    const char * toString();

    HRESULT predict( int iSizeObs, double *adObs, double *adPred );
    HRESULT predict( int iSizeObs, int iSizeTrees, double *adObs, double *adPred );
    HRESULT predictAndSave( int iSizeObs, int iSizeForest, double *adY, double *adX, std::string sOutFileFrst );
    HRESULT predictAndSave2( int iSizeObs, int iSizeForest, double *adY, double *adX, std::string sOutFileFrst );

    int         mTreesSize ;
    CMdlTree ** mTrees ;
};


#endif /* _F021_CLS_TMPL_H_ */
