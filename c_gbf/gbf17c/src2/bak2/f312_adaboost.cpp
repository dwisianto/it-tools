// GBM by Greg Ridgeway  Copyright (C) 2003

#include "f311_adaboost.h"

CAdaBoost::CAdaBoost()
{
}

CAdaBoost::~CAdaBoost()
{
}


HRESULT CAdaBoost::ComputeWorkingResponse
(
    double *adY,
    double *adMisc,
    double *adOffset,
    double *adF, 
    double *adZ, 
    double *adWeight,
    bool *afInBag,
    unsigned long nTrain
)
{
    unsigned long i = 0;

    assert(adY != NULL);
    assert(adF != NULL);
    assert(adZ != NULL);
    assert(adWeight != NULL);

    if(adOffset == NULL)
    {
        for(i=0; i<nTrain; i++)
        {
            adZ[i] = -(2*adY[i]-1) * exp(-(2*adY[i]-1)*adF[i]);
        }
    }
    else
    {
        for(i=0; i<nTrain; i++)
        {
            adZ[i] = -(2*adY[i]-1) * exp(-(2*adY[i]-1)*(adOffset[i]+adF[i]));
        }
    }

    return S_OK;
}



HRESULT CAdaBoost::InitF
(
    double *adY,
    double *adMisc,
    double *adOffset, 
    double *adWeight,
    double &dInitF, 
    unsigned long cLength
)
{
    dInitF = 0.0;

    return S_OK;
}


double CAdaBoost::LogLikelihood
(
    double *adY,
    double *adMisc, 
    double *adOffset, 
    double *adWeight,
    double *adF,
    unsigned long cLength
)
{
    unsigned long i=0;
    double dL = 0.0;

    assert(adY != NULL);
    assert(adWeight != NULL);
    assert(adF != NULL);

    if(adOffset == NULL)
    {
        for(i=0; i<cLength; i++)
        {
            dL += adWeight[i] * exp(-(2*adY[i]-1)*adF[i]);
        }
    }
    else
    {
        for(i=0; i<cLength; i++)
        {
            dL += adWeight[i] * exp(-(2*adY[i]-1)*(adOffset[i]+adF[i]));
        }
    }

    return dL;
}


HRESULT CAdaBoost::FitBestConstant
(
    double *adY,
    double *adMisc,
    double *adOffset,
    double *adW,
    double *adF,
    double *adZ,
    unsigned long *aiNodeAssign,
    unsigned long nTrain,
    VEC_P_NODETERMINAL vecpTermNodes,
    unsigned long cTermNodes,
    unsigned long cMinObsInNode,
    bool *afInBag,
    double *adFadj
)
{
    HRESULT hr = S_OK;

    double dF = 0.0;
    unsigned long iObs = 0;
    unsigned long iNode = 0;
    vecdNum.resize(cTermNodes);
    vecdNum.assign(vecdNum.size(),0.0);
    vecdDen.resize(cTermNodes);
    vecdDen.assign(vecdDen.size(),0.0);

    
    for(iObs=0; iObs<nTrain; iObs++)
    {
        if(afInBag[iObs])
        {
            dF = adF[iObs] + ((adOffset==NULL) ? 0.0 : adOffset[iObs]);
            vecdNum[aiNodeAssign[iObs]] += 
                adW[iObs]*(2*adY[iObs]-1)*exp(-(2*adY[iObs]-1)*dF);
            vecdDen[aiNodeAssign[iObs]] += 
                adW[iObs]*exp(-(2*adY[iObs]-1)*dF);
        }
    }

    for(iNode=0; iNode<cTermNodes; iNode++)
    {
        if(vecpTermNodes[iNode]!=NULL)
        {
            if(vecdDen[iNode] == 0)
            {
                vecpTermNodes[iNode]->dPrediction = 0.0;
            }
            else
            {
                vecpTermNodes[iNode]->dPrediction = 
                    vecdNum[iNode]/vecdDen[iNode];
            }
        }
    }

    return hr;
}


double CAdaBoost::BagImprovement
(
    double *adY,
    double *adMisc,
    double *adOffset,
    double *adWeight,
    double *adF,
    double *adFadj,
    bool *afInBag,
    double dStepSize,
    unsigned long nTrain
)
{
    double dReturnValue = 0.0;
    double dF = 0.0;
    unsigned long i = 0;

    for(i=0; i<nTrain; i++)
    {
        if(!afInBag[i])
        {
            dF = adF[i] + ((adOffset==NULL) ? 0.0 : adOffset[i]);
            
            dReturnValue += adWeight[i]*
                (exp(-(2*adY[i]-1)*dF) -
                 exp(-(2*adY[i]-1)*(dF+dStepSize*adFadj[i])));
        }
    }

    return dReturnValue;
}
