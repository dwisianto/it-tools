//============================================================================
// Name        : gbf17c.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>

#include "gbm.h"
#include "gbm_engine.h"
#include "gaussian.h"
//#include "f711_model_tree.h"
//#include "f712_model_forest.h"


#include "src1/gbf1a_common.hpp"
#include "src1/gbf1b_settings.hpp"
#include "src1/gbf1c_logging.hpp"
#include "src1/gbf1d_routine.hpp"






/**
 * main1 :
 */
int main1(void) {
	puts("Hello World!!!");
	return EXIT_SUCCESS;
}



/**
 * enum-based switch
 */
int main2(void) {
	puts("Hello World!!!");

	enum Action { hwConf, hwLog };
	Action ActNow = hwConf;
	switch(ActNow) {
	case hwConf: puts("hwConf"); break;
	case hwLog: puts("hwLog"); break;
	}

	puts("Bye World!!!");
	return EXIT_SUCCESS;
}



/*
 * Configuration Settin gs
 */
int main3(void) {

	puts("Hello World!!!");

	puts("hwConf");
	gbf_settings cfg;
	std::string sCfgName="conf/c3.xml";
	cfg.load(sCfgName);
	cfg.save(sCfgName+".tmp");

	puts("Bye World!!!");
	return EXIT_SUCCESS;
}


/**
 * main4 : logging
 */
int main4Logging(void) {
	puts("Hello World!!!");

	// [] Logging
	std::string initFileName = "conf/c3.properties";
	log4cpp::PropertyConfigurator::configure(initFileName);

// LOG_EMERG
// LOG_ALERT
// LOG_CRIT
// LOG_ERROR
// LOG_WARN
// LOG_NOTICE
// LOG_INFO
// LOG_DEBUG

	GBFRoutine *routine = new GBFRoutine();
	routine->mLog.emerg("9 Emergency");
	routine->mLog.fatal("8 fatal");
	routine->mLog.alert("7 Alert");
	routine->mLog.crit("6 crit");
	routine->mLog.error("5 Error");
	routine->mLog.warn("4 Warn");
	routine->mLog.notice("3 Notice");
	routine->mLog.info("2 Info");
	routine->mLog.debug("1 Debug");

	routine->mLogRoot.emerg("Root 9 Emergency");
	routine->mLogRoot.fatal("Root 8 Fatal");
	routine->mLogRoot.crit("Root 7 Critical");
	routine->mLogRoot.alert("Root 6 Alert");
	routine->mLogRoot.error("Root 5 Error");
	routine->mLogRoot.warn("Root 4 Warn");
	routine->mLogRoot.notice("Root 3 Notice");
	routine->mLogRoot.info("Root 2 Info");
	routine->mLogRoot.debug("Root 1 debug");

	// []
	routine->mLog.info("Bye");
	log4cpp::Category::shutdown();

	return EXIT_SUCCESS;
}

/**
 * main5 : routine steps
 */
int main5() {

	// [] Logging
	std::string initFileName = "conf/c3.properties";
	log4cpp::PropertyConfigurator::configure(initFileName);

	// [] Configuration
	gbf_settings cfg;
	std::string sCfgName="conf/c3.xml";
	cfg.load(sCfgName);
	cfg.save(sCfgName+".tmp");

	// []
	GBFRoutine *routine = new GBFRoutine();
	routine->mLog.info("Forrest Run ");

	enum Action { hwPrnt, hwSort, hwSortMat,
	dbInit64a, dbInit64b, dbRead,
	fRun1c};
	Action ActNow = dbRead;
	switch(ActNow) {
	case hwPrnt:    routine->t1a_print_vector();      break;
	case hwSort:    routine->t1b_sort_index_array();  break;
	case hwSortMat: routine->t1c_sort_index_matrix(); break;
	case dbInit64a: routine->t1d_init_data46(); break;
	case dbInit64b: routine->t1e_init_data46(); break;
	case dbRead:    routine->t1f_db_read_try(); break;
	case fRun1c:    routine->t3a(cfg);break;
	}


	// []
	/*
	std::string sCase="t2c";
	if ( sCase == "t1a")      { tt->t1a_print_vector(); }
	else if ( sCase == "t1b") { tt->t1b_sort_index_array(); }
	else if ( sCase == "t1c") { tt->t1c_sort_index_matrix(); }
	else if ( sCase == "t1d") { tt->t1d_init_data46(); }
	else if ( sCase == "t1e") { tt->t1e_init_data46(); }
	else if ( sCase == "t1f") { tt->t1f_db_read_try(); }
	else if ( sCase == "t2a") { tt->t2a_gbm_run(); }
	else if ( sCase == "t2b") { tt->t2b_gbm_run(); }
	else if ( sCase == "t2c") { tt->t2c_gbf(cfg); }
	*/

	// []
	routine->mLog.info("Bye");
	log4cpp::Category::shutdown();

	// []
	return 0;
}


/**
 * main6Run
 * forest run
 */
int main() {

	// [] Logging
	std::string initFileName = "conf/c3.properties";
	log4cpp::PropertyConfigurator::configure(initFileName);

	// [] Configuration
	gbf_settings cfg;
	std::string sCfgName="conf/c3.xml";
	cfg.load(sCfgName);
	cfg.save(sCfgName+".tmp");

	// []
	GBFRoutine *routine = new GBFRoutine();
	routine->mLog.info("Forrest Run ");
	routine->t3a(cfg);


	// []
	routine->mLog.info("Bye");
	log4cpp::Category::shutdown();

	// []
	return 0;
}

