import numpy as np
import timeit


def skipgram_ndarray(sent, k=1, n=2):
    """
        k 
    """
    tokens = sent.split()
    if len(tokens) < k + 2:
        raise Exception("REQ: length of sentence > skip + 2")
    #print( len(tokens) )
    
    matrix = np.zeros((len(tokens), k + 2), dtype=object)
    matrix[:, 0] = tokens
    matrix[:, 1] = tokens[1:] + ['']
    #print( matrix.shape )
    #print( matrix[:,0] )
    #print( matrix[:,1] )
    
    
    for skip in range(1, k + 1):
        matrix[:, skip + 1] = tokens[skip + 1:] + [''] * (skip + 1)
        #print(matrix[:, skip + 1])
        
    result = []
    for index in range(1, k + 2):
        print( matrix[:,0])
        print( matrix[:,index])
        temp = matrix[:, 0] + ',' + matrix[:, index]
        result.append(temp)
        #map(result.append, temp)
        #map(result.append, temp.tolist())
        print (result)
    '''    
    #limit = (((k + 1) * (k + 2)) / 6) * ((3 * n) - (2 * k) - 6)
    #limit = int(((k + 1) * (k + 2)) / 6) * ((3 * n) - (2 * k) )
    #print (limit)
    #print result[:limit])
    #print(result)    
    #return result[:limit]    
    #return result
    '''
    return 0



if __name__ == "__main__":
    text = """Pretty awesome except for the mate choice data. """
    skipgram_ndarray(text, 1, 2)
    
    
    '''            
    print("```````````````````````````````````````````````````````````````")
    loops_list = []
    timer_lc = []
    timer_ar = []
    for index in range(3):
        #loops = 100 * 10 ** index
        loops = index
        loops_list.append(loops)
        timer_ar.append(timeit.timeit(lambda: skipgram_ndarray(text, 1, 2),number=loops))        
#        timer_lc.append(timeit.timeit(lambda: skipgram_list(text, 1, 2),number=loops))                        

                        
    print( "Loops, List comprehensions, ndarray\n")
    for index in range(len(loops_list)):
        print( "{0}, {1}".format(loops_list[index], timer_ar[index]) )
        #print( "{0}, {1}, {2}".format(loops_list[index], timer_lc[index],timer_ar[index]) )
                                     
    '''